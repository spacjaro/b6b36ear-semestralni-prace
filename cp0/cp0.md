# Informační systém pro půjčovnu tiskařských strojů

## Úvod

Tento informační systém (IS) je navržen pro firmu, která se specializuje na pronájem tiskařských strojů.

## Účel

Cílem systému je urychlit a zefektivnit proces správy zákazníků, evidování jejich strojů, sledování stavu strojů, počtu výtisků a následné generování a odesílání faktur.

## Pro koho je IS určen

Systém je určen zaměstnancům a administrátorům firmy, která pronajímá tiskařské stroje, a také zákazníkům, kteří si stroje pronajímají.

## Funkcionality

### Mechanik / Admin

- **Přidávání/Odebírání strojů:** Přidávání nebo odebírání strojů jednotlivým klientům.
- **Záznamy o odpočtech:** Vytváření záznamů o odpočtech (počty výtisků) jednotlivých strojů.
- **Přidávání nových strojů:** Přidávání nových tiskařských strojů do systému.
- **Přidávání oprav na fakturu:** Přidávání nákladů na opravy strojů do faktury klienta.
- **Generování faktur:** Automatické generování faktur na základě užívání strojů a souvisejících služeb.
- **Odesílání faktur:** Zasílání faktur klientům e-mailem nebo jiným způsobem.

### Admin

- **Správa modelů/brandů:** Přidávání nebo odebírání modelů a značek tiskařských strojů.
- **Správa klientů:** Přidávání nebo odebírání klientů ze systému.

### Klient

- **Sledování stavu strojů:** Sledování stavu pronajatých strojů v reálném čase.
- **Předběžné fakturační údaje:** Zobrazení předběžných fakturačních údajů, jako je spotřeba a nadcházející poplatky.

## UML Diagram

![UML Diagram tříd](AMD.png)


---

Tento soubor README poskytuje přehled klíčových funkcí systému a cílové skupiny uživatelů. Podrobné pokyny pro instalaci a provoz systému budou uvedeny v samostatné dokumentaci.
