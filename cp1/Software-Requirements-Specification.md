# Software Requirements Specification (SRS)

## 1. Introduction
Tento dokument poskytuje podrobný popis informačního systému pro půjčovnu tiskařských strojů. Cílem systému je urychlit a zefektivnit proces správy zákazníků, evidování jejich strojů, sledování stavu strojů, počtu výtisků a následné generování a odesílání faktur.

## 2. Intended Users
- **Mechanik**: Přidávání a odebírání strojů, vytváření záznamů o odpočtech, přidávání nových strojů, přidávání oprav na fakturu, generování a odesílání faktur.
- **Admin**: Má všechny funkce co Mechanik, a také správu modelů a značek tiskařských strojů, správu klientů.
- **Klient**: Sledování stavu pronajatých strojů v reálném čase, zobrazení předběžných fakturačních údajů.

## 3. System Functions
### Mechanik / Admin
- **Přidávání/Odebírání strojů:** Přidávání nebo odebírání strojů jednotlivým klientům.
- **Záznamy o odpočtech:** Vytváření záznamů o odpočtech (počty výtisků) jednotlivých strojů.
- **Přidávání nových strojů:** Přidávání nových tiskařských strojů do systému.
- **Přidávání oprav na fakturu:** Přidávání nákladů na opravy strojů do faktury klienta.
- **Generování faktur:** Automatické generování faktur na základě užívání strojů a souvisejících služeb.
- **Odesílání faktur:** Zasílání faktur klientům e-mailem nebo jiným způsobem.

### Admin
- **Správa modelů/brandů:** Přidávání nebo odebírání modelů a značek tiskařských strojů.
- **Správa klientů:** Přidávání nebo odebírání klientů ze systému.

### Klient
- **Sledování stavu strojů:** Sledování stavu pronajatých strojů v reálném čase.
- **Předběžné fakturační údaje:** Zobrazení předběžných fakturačních údajů, jako je spotřeba a nadcházející poplatky.

## 4. System Constraints
- Systém nebude podporovat sledování strojů v reálném čase.
- Systém nebude zpracovávat platby.

## 5. Object Model (UML Class Diagram)
![UML Diagram](UML.png)

## 6. Authentication and Other Constraints
- Uživatelé se budou autentizovat pomocí uživatelského jména a hesla.
- Pouze autentizovaní uživatelé budou mít přístup k určitým funkcím.

## 7. Implementation Plan
### Phase 1: User Authentication
- Implementace autentizace uživatelů pomocí uživatelského jména a hesla.
- Nastavení rolí a oprávnění pro různé typy uživatelů (Admin, Mechanik, Klient).

### Phase 2: Basic CRUD Operations
- Implementace základních CRUD operací pro správu strojů (přidávání, odebírání, aktualizace a zobrazení strojů).
- Implementace základních CRUD operací pro správu uživatelů (přidávání, odebírání, aktualizace a zobrazení uživatelů).

### Phase 3: Machine Management
- Implementace funkcionality pro přidávání a odebírání strojů jednotlivým klientům.
- Implementace vytváření záznamů o odpočtech (počty výtisků) jednotlivých strojů.
- Implementace přidávání nových tiskařských strojů do systému.

### Phase 4: Repair and Billing Management
- Implementace přidávání nákladů na opravy strojů do faktury klienta.
- Implementace automatického generování faktur na základě užívání strojů a souvisejících služeb.
- Implementace zasílání faktur klientům e-mailem nebo jiným způsobem.

### Phase 5: Client Functionality
- Implementace sledování stavu pronajatých strojů pro klienty.
- Implementace zobrazení předběžných fakturačních údajů pro klienty.

### Phase 6: Admin Functionality
- Implementace správy modelů a značek tiskařských strojů.
- Implementace správy klientů (přidávání nebo odebírání klientů ze systému).

### Phase 7: Testing and Deployment
- Testování všech implementovaných funkcionalit.
- Nasazení systému do produkčního prostředí.
- Školení uživatelů a administrátorů systému.