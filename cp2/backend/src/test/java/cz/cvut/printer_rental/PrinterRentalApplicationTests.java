// package cz.cvut.printer_rental;
//
// import cz.cvut.printer_rental.service.CustomerService;
// import cz.cvut.printer_rental.service.ModelService;
// import cz.cvut.printer_rental.service.PrinterService;
// import cz.cvut.printer_rental.model.Brand;
// import cz.cvut.printer_rental.model.PrinterModel;
// import org.junit.jupiter.api.BeforeEach;
// import org.junit.jupiter.api.Test;
// import org.springframework.beans.factory.annotation.Autowired;
// import org.springframework.boot.test.context.SpringBootTest;
//
// import java.util.List;
//
// import static
// org.assertj.core.api.FactoryBasedNavigableListAssert.assertThat;
//
// @SpringBootTest
// class PrinterRentalApplicationTests {
//
// @Autowired
// private PrinterService printerService;
//
// @Autowired
// private CustomerService customerService;
//
// @Autowired
// private ModelService modelService;
//
// private DataGenerator dataGenerator;
//
// @BeforeEach
// public void setUp() {
// dataGenerator = new DataGenerator();
// }
//
//
//
// @Test
// public void testGenerateAndSavePrinterModels() {
// // Generate brands
// List<Brand> brands = dataGenerator.generateBrands();
// brands.forEach(brandService::save);
//
// // Generate printer models for the first brand
// List<PrinterModel> printerModels =
// dataGenerator.generatePrinterModels(brands.get(0));
//
// // Save printer models using the service
// printerModels.forEach(ModelService::save);
//
// // Retrieve printer models from the database
// List<PrinterModel> savedModels = ModelService.findAll();
//
// // Assert that the saved models match the generated models
// assertThat(savedModels).hasSize(printerModels.size());
// assertThat(savedModels).extracting("modelName").contains(
// brands.get(0).getBrandName() + "_PrinterModel_0",
// brands.get(0).getBrandName() + "_PrinterModel_1",
// brands.get(0).getBrandName() + "_PrinterModel_2"
// );
// }
//
// @Test
// public void testGenerateAndSavePrinters() {
// // Generate brands and printer models
// List<Brand> brands = dataGenerator.generateBrands();
// brands.forEach(brandService::save);
//
// List<PrinterModel> printerModels =
// dataGenerator.generatePrinterModels(brands.get(0));
// printerModels.forEach(printerModelService::save);
//
// // Generate printers based on models
// List<Printer> printers = dataGenerator.generatePrinters(printerModels);
//
// // Save printers using the service
// printers.forEach(printerService::save);
//
// // Retrieve printers from the database
// List<Printer> savedPrinters = printerService.findAll();
//
// // Assert that the saved printers match the generated printers
// assertThat(savedPrinters).hasSizeGreaterThan(0);
// assertThat(savedPrinters).extracting("serialNumber").isNotEmpty();
// }
//
// @Test
// public void testGenerateAndSaveCustomers() {
// // Generate customers
// List<Customer> customers = dataGenerator.generateCustomers();
//
// // Save customers using the service
// customers.forEach(customerService::save);
//
// // Retrieve customers from the database
// List<Customer> savedCustomers = customerService.findAll();
//
// // Assert that the saved customers match the generated customers
// assertThat(savedCustomers).hasSize(customers.size());
// assertThat(savedCustomers).extracting("email").contains(
// "firma1@example.com",
// "osoba1@example.com"
// );
// }
//
// }
