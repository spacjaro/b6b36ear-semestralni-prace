package cz.cvut.printer_rental.model;

import lombok.Data;

public enum PaperSize {
    A5(148, 210),
    A4(210, 297),
    A3(297, 420),
    A2(420, 594);


    private final int width;
    private final int height;

    private PaperSize(int width, int height) {
        this.width = width;
        this.height = height;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    @Override
    public String toString() {
        return name() + " (" + width + "mm x " + height + "mm)";
    }
}
