package cz.cvut.printer_rental.model;

public enum Role {
  ADMIN("ADMIN"), MECHANIC("MECHANIC"), SUPER_ADMIN("SUPER_ADMIN");

  private final String name;

  Role(String name) {
    this.name = name;
  }

  @Override
  public String toString() {
    return name;
  }
}
