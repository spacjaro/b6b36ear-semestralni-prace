package cz.cvut.printer_rental.rest.util;

import cz.cvut.printer_rental.model.Customer;

import java.util.Collections;
import java.util.List;

public class CustomerUtils {

  public static List<Customer> getCustomerList(Customer customer) {
    return customer != null ? List.of(customer) : Collections.emptyList();
  }
}