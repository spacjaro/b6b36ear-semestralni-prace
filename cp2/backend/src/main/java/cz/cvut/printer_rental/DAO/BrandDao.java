package cz.cvut.printer_rental.dao;

import cz.cvut.printer_rental.model.Brand;
import jakarta.persistence.NoResultException;
import org.springframework.stereotype.Repository;

import java.util.Objects;

@Repository
public class BrandDao extends BaseDao<Brand> {
  public BrandDao() {
    super(Brand.class);
  }

  public Brand findByName(String brandName) {
    Objects.requireNonNull(brandName);
    try {
      return em.createNamedQuery("Brand.findByName", Brand.class)
          .setParameter("brandName", brandName)
          .getSingleResult();
    } catch (NoResultException e) {
      return null; // Místo vyhození výjimky vracíme null
    }
  }
}
