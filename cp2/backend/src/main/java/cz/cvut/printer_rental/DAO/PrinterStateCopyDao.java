package cz.cvut.printer_rental.dao;

import cz.cvut.printer_rental.model.PrinterStateCopy;
import org.springframework.stereotype.Repository;

@Repository
public class PrinterStateCopyDao extends BaseDao<PrinterStateCopy> {
  public PrinterStateCopyDao() {
    super(PrinterStateCopy.class);
  }
}
