package cz.cvut.printer_rental.service;

import cz.cvut.printer_rental.dao.BaseDao;
import cz.cvut.printer_rental.dao.CustomerDao;
import cz.cvut.printer_rental.dao.InvoiceDao;
import cz.cvut.printer_rental.dao.QuarterYearDao;
import cz.cvut.printer_rental.model.Customer;
import cz.cvut.printer_rental.model.Invoice;
import cz.cvut.printer_rental.model.Printer;
import cz.cvut.printer_rental.model.QuarterYear;
import cz.cvut.printer_rental.model.Customer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
public class InvoiceService extends AbstractCrudService<Invoice> {
  private static final Logger logger = LoggerFactory.getLogger(InvoiceService.class);

  private final InvoiceDao invoiceDao;
  private final QuarterYearDao quarterYearDao;
  private final CustomerDao customerDao;

  private final QuarterYearService quarterYearService;

  @Autowired
  public InvoiceService(
      InvoiceDao invoiceDao,
      QuarterYearDao quarterYearDao,
      CustomerDao customerDao,
      QuarterYearService quarterYearService) {
    this.quarterYearDao = quarterYearDao;
    this.customerDao = customerDao;
    this.invoiceDao = invoiceDao;
    this.quarterYearService = quarterYearService;
  }

  @Override
  protected BaseDao<Invoice> selfDao() {
    return invoiceDao;
  }

  @Transactional
  public void deleteById(Integer id) {
    try {
      Invoice invoice = invoiceDao.find(id);
      super.delete(invoice);
    } catch (Exception e) {
      logger.error(e.getMessage());
      throw e;
    }
  }

  @Transactional
  public List<Invoice> findInvoicesByCustomerId(Integer customerId) {
    try {
      Customer customer = customerDao.find(customerId);
      Objects.requireNonNull(customer);
      return invoiceDao.findByCustomer(customer);
    } catch (Exception e) {
      logger.warn("Error finding invoices for customer: {}", customerId, e);
      return null;
    }
  }

  @Transactional
  public List<Invoice> findInvoicesByQuarterYearId(Integer quarterYearId) {
    try {
      QuarterYear quarterYear = quarterYearDao.find(quarterYearId);
      Objects.requireNonNull(quarterYear);
      return invoiceDao.findByQuarterYear(quarterYear);
    } catch (Exception e) {
      logger.error("Error finding invoices for quarter year: {}", quarterYearId, e);
      return null;
    }
  }

  @Transactional
  public List<Invoice> findUnpaidInvoices() {
    try {
      return invoiceDao.findUnpaid();
    } catch (Exception e) {
      logger.error("Error finding unpaid invoices.", e);
      return null;
    }
  }

  @Transactional
  public List<Invoice> findPaidInvoices() {
    try {
      return invoiceDao.findPaid();
    } catch (Exception e) {
      logger.error("Error finding paid invoices.", e);
      return null; // or handle exception properly
    }
  }

  /*
   * This should find Invoice any time if the printer id in poses of some Customer
   */
  @Transactional
  public Optional<Invoice> findActualInvoiceForPrinter(Printer printer) {
    try {
      Objects.requireNonNull(printer);
      // Find all Invoice by Customer and return actual one.
      return invoiceDao.findByCustomer(printer.getCustomer()).stream().filter(Invoice::isActual).findAny();
    } catch (Exception e) {
      logger.error("Find actual Invoice for Customer failed due to: {}", e.getMessage());
      throw e;
    }
  }

  // Funkce ktera pri vytvoreni nveho Customer automaticky vytvori novou Invoice
  public void initialInvoiceAssigment(Customer customer) {
    try {
      Invoice invoice = new Invoice(customer);
      QuarterYear curQuarterYear = quarterYearService.getCurrentQuarterYear();
      invoice.setQuarterYear(curQuarterYear);
      super.save(invoice);
    } catch (Exception e) {
      logger.error("Failed to initialize invoice assigment for customer {} EXCEPTION:  {}", customer.getId(),
          e.getMessage());
      throw e;
    }
  }
}
