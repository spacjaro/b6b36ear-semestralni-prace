package cz.cvut.printer_rental.dao;

import cz.cvut.printer_rental.model.Customer;
import cz.cvut.printer_rental.model.Invoice;
import cz.cvut.printer_rental.model.Model;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Objects;

@Repository
public class ModelDao extends BaseDao<Model> {
  public ModelDao() {
    super(Model.class);
  }

  /**
   * Finds a model by its EAN (European Article Number).
   *
   * @param EAN the EAN of the model to be retrieved.
   * @return the model with the given EAN, or null if not found or in case of an
   *         error.
   */
  public Model findByEAN(String EAN) {
    try {
      return em.createNamedQuery("Model.findByEAN", Model.class)
          .setParameter("EAN", EAN)
          .getSingleResult();
    } catch (Exception e) {
      return null; // Return null in case of error or if the model is not found
    }
  }

  /**
   * Finds a model by its model name.
   *
   * @param ModelName the model name to be retrieved.
   * @return the model with the given model name, or null if not found or in case
   *         of an error.
   */
  public Model findByModelName(String ModelName) {
    try {
      return em.createNamedQuery("Model.findByModelName", Model.class)
          .setParameter("MODEL_NAME", ModelName)
          .getSingleResult();
    } catch (Exception e) {
      return null; // Return null in case of error or if the model is not found
    }
  }

}
