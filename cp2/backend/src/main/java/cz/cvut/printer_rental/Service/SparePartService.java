package cz.cvut.printer_rental.service;

import cz.cvut.printer_rental.dao.BaseDao;
import cz.cvut.printer_rental.dao.SparePartDao;
import cz.cvut.printer_rental.model.*;
import jakarta.persistence.EntityNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Objects;

@Service
public class SparePartService extends AbstractCrudService<SparePart> {
  private static final Logger logger = LoggerFactory.getLogger(SparePartService.class);
  private final SparePartDao sparePartDao;
  private final ModelService modelService;
  private final PrinterService printerService;

  @Autowired
  public SparePartService(SparePartDao sparePartDao, ModelService modelService, PrinterService printerService) {
    this.sparePartDao = sparePartDao;
    this.modelService = modelService;
    this.printerService = printerService;
  }

  @Override
  protected BaseDao<SparePart> selfDao() {
    return sparePartDao;
  }

  @Override
  @Transactional
  public void save(SparePart entity) {
    try {
      super.save(entity);
    } catch (Exception e) {
      logger.error(e.getMessage());
      throw e;
    }
  }

  @Override
  @Transactional
  public SparePart findById(Integer id) {
    try {
      return super.findById(id);
    } catch (Exception e) {
      logger.error(e.getMessage());
      return null;
    }
  }

  @Override
  @Transactional
  public List<SparePart> findAll() {
    try {
      return super.findAll();
    } catch (Exception e) {
      logger.error(e.getMessage());
      return null;
    }
  }

  @Override
  @Transactional
  public void update(SparePart entity) {
    try {
      super.update(entity);
    } catch (Exception e) {
      logger.error(e.getMessage());
      throw e;
    }
  }

  @Override
  @Transactional
  public void delete(SparePart entity) {
    try {
      super.delete(entity);
    } catch (Exception e) {
      logger.error(e.getMessage());
      throw e;
    }
  }

  @Transactional
  public List<SparePart> findSparePartsByModel(Integer modelId) {
    try {
      SparePartModel model = modelService.getSparePartModelByID(modelId);
      return sparePartDao.findByModel(model);
    } catch (Exception e) {
      logger.error("Error occurred while finding spare parts by model ID: {}", modelId);
      return null; // Return null or handle as needed
    }
  }

  @Transactional
  public List<SparePart> findSparePartsForPrinterModel(Integer modelId) {
    try {
      PrinterModel model = modelService.getPrinterModelByID(modelId);
      return sparePartDao.findCompatibleForPrinterModel(model);
    } catch (Exception e) {
      logger.error("Error occurred while finding spare parts for printer model ID: {}", modelId);
      return null;
    }
  }

  @Transactional
  public List<SparePart> findSparePartsForPrinter(Integer printerId) {
    try {
      Printer printer = printerService.findById(printerId);
      return findSparePartsForPrinterModel(printer.getPrinterModel().getId());
    } catch (Exception e) {
      logger.error("Error occurred while finding spare parts for printer ID: {}", printerId);
      return null;
    }
  }

  public void increaseAmount(int amount, Integer sparePartID) {
    Objects.requireNonNull(sparePartID);
    try {
      SparePart part = sparePartDao.find(sparePartID);
      part.increaseAmount(amount);
      sparePartDao.update(part);
    } catch (IllegalArgumentException e) {
      logger.warn(e.getMessage());
      throw e;
    }
  }

  public void decreaseAmount(int amount, Integer sparePartID) {
    Objects.requireNonNull(sparePartID);
    try {
      SparePart part = sparePartDao.find(sparePartID);
      part.decreaseAmount(amount);
      sparePartDao.update(part);
    } catch (IllegalArgumentException e) {
      logger.warn(e.getMessage());
      throw e;
    } catch (Exception e) {
      logger.error(e.getMessage());
      throw e;
    }
  }

  @Transactional
  public void setAmount(int amount, SparePart sparePart) {
    try {
      sparePart.setAmount(amount);
      super.update(sparePart);
    } catch (Exception e) {
      logger.error(e.getMessage());
      throw e;
    }
  }
}
