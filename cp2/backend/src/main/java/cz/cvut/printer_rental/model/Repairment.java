package cz.cvut.printer_rental.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import jakarta.persistence.*;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@EqualsAndHashCode(callSuper = true)
@Entity
@Data
@Table(name = "REPAIRMENT")
public class Repairment extends AbstractEntity{
    @Column
    private String description;

    @Column(name = "date")
    @Basic(optional = false)
    private LocalDateTime dateOfCreation;

    @Column(name = "totalPrice")
    private double totalPrice;

    @Column(name = "workPrice")
    private double workPrice;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonBackReference
    private List<RepairItem> repairItems;

    public Repairment() {
        this.repairItems = new ArrayList<>();
        this.totalPrice = 0.0;
        this.workPrice = 0.0;
        this.description = "";
        this.dateOfCreation = LocalDateTime.now();
    }

    public void setWorkPrice(double workPrice) {
        if (workPrice < 0) {
            throw new IllegalArgumentException("Total price must be greater than zero");
        }else {
            this.workPrice = workPrice;
        }
    }

    public void addRepairItem(RepairItem repairItem) {
        Objects.requireNonNull(repairItem);
        if(repairItems == null){
            repairItems = new ArrayList<>();
        }
        final Optional<RepairItem> exist = repairItems.stream().filter(r -> r.getId().equals(repairItem.getId())).findAny();
        if (exist.isPresent()) {
            throw new IllegalArgumentException("Repair item already exists");
        }else{
            repairItems.add(repairItem);
        }
    }
    public void removeRepairment(RepairItem repairItem) {
        Objects.requireNonNull(repairItem);
        final Optional<RepairItem> exist = repairItems.stream().filter(r -> r.getId().equals(repairItem.getId())).findAny();
        if (exist.isPresent()) {
            repairItems.remove(repairItem);
        }else{
            throw new IllegalArgumentException("Repair item does not exist");
        }
    }

    @Override
    public String toString() {
        return "\n" + "Repairment{ " + '\n' +
                "       description='" + description + '\n' +
                "       dateOfCreation=" + dateOfCreation + '\n' +
                ",      totalPrice=" + totalPrice + '\n' +
                ",      workPrice=" + workPrice + '\n' +
                ",      repairItems=" + repairItems.stream()
                .map(item -> String.valueOf(item.getId()))
                .toList() +
                '}';
    }

}



