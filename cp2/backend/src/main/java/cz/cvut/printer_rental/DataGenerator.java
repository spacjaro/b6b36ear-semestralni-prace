package cz.cvut.printer_rental;

import cz.cvut.printer_rental.model.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

public class DataGenerator {
    public static class EANGenerator {


        private static long lastEAN = 1000000000L;


        public static Long generateEAN() {
            lastEAN++;

            return lastEAN;
        }
    }
    Random rand = new Random();

    //Bez Models

    public List<Brand> generateBrands() {
        List<String> brandsName = Arrays.asList("Canon", "HP", "Minolta");
        return brandsName.stream()
                .map(name -> {
                    Brand brand = new Brand();
                    brand.setBrandName(name);
                    return brand;
                })
                .collect(Collectors.toList());
    }


    public List<PrinterModel> generatePrinterModels(Brand brand) {
        Random rand = new Random();
        List<PrinterModel> models = new ArrayList<>();
        for (int i = 0; i < rand.nextInt(2,5); i++) {
            PrinterModel model = new PrinterModel();
            model.setBrand(brand);
            model.setModelName(brand.getBrandName() +"_PrinterModel_"+ String.valueOf(i));
            model.setDescription(brand.getBrandName()+" Printer model "+String.valueOf(i)+" Description" );
            model.setEAN(String.valueOf(EANGenerator.generateEAN()));
            model.setIsColor(rand.nextBoolean());
            model.setIsDuplex(rand.nextBoolean());
            model.setIsScanner(rand.nextBoolean());
            model.setIsWireless(rand.nextBoolean());
            // Randomly select paper sizes using the PaperSize enum
            List<PaperSize> availablePaperSizes = List.of(PaperSize.values());
            List<PaperSize> randomPaperSizes = availablePaperSizes.stream()
                    .filter(size -> rand.nextBoolean())
                    .toList();
            model.setPaperSize(randomPaperSizes);

            model.setPrintSpeed(rand.nextDouble(10,50));
            model.setResolution(rand.nextInt(1000));
            models.add(model);
        }
        return models;
    }
    public List<Printer> generatePrinters(List<PrinterModel> models) {
        Random rand = new Random();
        List<Printer> printers = new ArrayList<>();
        for (PrinterModel model: models) {
            int rand_int1 = rand.nextInt(2,5);
            for (int i = 0; i < rand_int1; i++) {
                Printer printer = new Printer();
                printer.setPrinterModel(model);
                printer.setBw_price_per_copy(3.5 + rand.nextDouble());
                printer.setColor_price_per_copy(5.0 + rand.nextDouble() * 2);
                printer.setNumOfBWCopy(rand.nextInt(1000,5000));
                printer.setCURRENTNumOfBWCopy(rand.nextInt(5000,8000));
                printer.setNumOfColorCopy(rand.nextInt(1000,3000));
                printer.setCURRENTNumOfColorCopy(rand.nextInt(3000,5000));
                printer.setRent_price(100 + rand.nextDouble(10,20) * 50);
                printer.setSerialNumber("SN-" + rand.nextInt(100000));
                printers.add(printer);
            }
        }
        return printers;
    }

    public List<SparePartModel> generateSparePartModels(Brand brand) {
        List<SparePartModel> models = new ArrayList<>();
        for (int i = 0; i < rand.nextInt(5,9); i++) {
            SparePartModel model = new SparePartModel();
            model.setBrand(brand);
            model.setModelName(brand.getBrandName() +"_SparePartModel_"+ String.valueOf(i));
            model.setDescription(brand.getBrandName()+" Spare Part model "+String.valueOf(i)+" Description" );
            models.add(model);
            model.setEAN(String.valueOf(EANGenerator.generateEAN()));
        }
        return models;
    }
    public List<SparePart> generateSpareParts(List<SparePartModel> models) {
        Random rand = new Random();
        List<SparePart> spareParts = new ArrayList<>();
        for (SparePartModel model: models) {
//            SparePart sparePart = new SparePart(model,rand.nextInt(5,20));
            SparePart sparePart = new SparePart(model,20);
            spareParts.add(sparePart);
        }
        return spareParts;
    }

    public List<Customer> generateCustomers() {
        List<Customer> customers = new ArrayList<>();

        String[] companyNames = {
                "Česká spořitelna a.s.",
                "Alza.cz a.s.",
                "Seznam.cz s.r.o.",
                "Škoda Auto a.s.",
                "ČEZ a.s.",
                "ZŠ Prof. Švejcara"
        };

        for (int i = 0; i < companyNames.length; i++) {
            Customer company = new Customer();
            company.setCompanyName(companyNames[i]);
            company.setIco("ICO" + (1000 + i));
            company.setDic("CZ" + (1000 + i));
            company.setEmail("firma" + (i+1) + "@example.com");
            company.setAddress(new Address("Ulice " + (i+1), "Praha", "11000", "CZ"));
            company.setPhoneNumber(new PhoneNumber(Integer.toString(420), Integer.toString(11122233*10+ i)));

            customers.add(company);
        }

        String[][] personNames = {
                {"Jan", "Novák"},
                {"Petra", "Svobodová"},
                {"Tomáš", "Dvořák"},
                {"Lucie", "Horáková"},
                {"Martin", "Procházka"}
        };

        for (int i = 0; i < personNames.length; i++) {
            Customer person = new Customer();
            person.setFirstName(personNames[i][0]);
            person.setLastName(personNames[i][1]);
            person.setEmail("osoba" + (i+1) + "@example.com");
            person.setAddress(new Address("Ulice " + (i+1), "Praha", "11000", "CZ"));
            person.setPhoneNumber(new PhoneNumber(Integer.toString(420), Integer.toString(31122233*10+ i)));

            customers.add(person);
        }

        return customers;
    }

    public List<QuarterYear> generateQuarterYears() {
        List<QuarterYear> quarterYears = new ArrayList<>();
        Month[] months = Month.values();
        for (int year = 2022; year < 2024; year++) {
            for (int month = 0; month < 12; month++) {
                QuarterYear quarterYear = new QuarterYear();
                quarterYear.setFirstMonth(months[month]);
                int next_month_index = month+2;
                if (next_month_index < 12) {
                    quarterYear.setCurrentYear(year);
                    quarterYear.setLastMonth(months[next_month_index]);
                }else{
                    quarterYear.setCurrentYear(year+1);
                    quarterYear.setLastMonth(months[next_month_index%12]);
                }
                quarterYears.add(quarterYear);
            }
        }
        return quarterYears;
    }



    public <T> void printList(List<T> list) {
        list.forEach(obj -> System.out.println(obj.toString()));
    }

    public static void main(String[] args) {
        DataGenerator dataGenerator = new DataGenerator();

        // Generování dat
        List<Brand> brands = dataGenerator.generateBrands();
        List<PrinterModel> printerModels = dataGenerator.generatePrinterModels(brands.get(0));
        List<SparePartModel> sparePartModels = dataGenerator.generateSparePartModels(brands.get(0));
        List<Customer> customers = dataGenerator.generateCustomers();
        List<QuarterYear> quarterYears = dataGenerator.generateQuarterYears();
        List<Printer> printers = dataGenerator.generatePrinters(printerModels);



        System.out.println("\033[34mSeznam značek:\033[0m\n");  // Blue for brands list
        dataGenerator.printList(brands);

        System.out.println("\033[32mSeznam Printer Modelů:\033[0m\n");  // Green for printer models list
        dataGenerator.printList(printerModels);

        System.out.println("\033[33mSeznam Spare Part Modelů:\033[0m\n");  // Yellow for spare part models list
        dataGenerator.printList(sparePartModels);

        System.out.println("\033[35mSeznam zákazníků:\033[0m\n");  // Magenta for customers list
        dataGenerator.printList(customers);

//        System.out.println("\033[36mSeznam čtvrtletí:\033[0m\n");  // Cyan for quarters list
//        dataGenerator.printList(quarterYears);

        System.out.println("\033[37mSeznam Printers:\033[0m\n");  // White for printers list
        dataGenerator.printList(printers);

    }




}

