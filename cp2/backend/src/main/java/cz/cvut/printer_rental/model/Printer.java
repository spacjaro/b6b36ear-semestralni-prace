package cz.cvut.printer_rental.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import jakarta.persistence.*;
import lombok.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Entity
@Data
@Builder
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Table(name = "PRINTERS")
@NoArgsConstructor

@NamedQueries({
        // Najde všechny tiskárny patřící konkrétnímu zákazníkovi
        @NamedQuery(name = "Printer.findByCustomer",
                query = "SELECT p FROM Printer p WHERE p.customer = :customer"),

        // Najde všechny tiskárny dle modelu
        @NamedQuery(name = "Printer.findByModel",
                query = "SELECT p FROM Printer p WHERE p.PrinterModel = :model"),

        // Najde všechny tiskárny dle značky
        @NamedQuery(name = "Printer.findByBrand",
                query = "SELECT p FROM Printer p WHERE p.PrinterModel.brand = :brand"),

        // Najde všechny volne tiskárny
        @NamedQuery(name = "Printer.findFree",
                query = "SELECT p FROM Printer p WHERE p.customer IS NULL "),
        @NamedQuery(name = "Printer.findBySerialNumber",
                query = "SELECT p FROM Printer p WHERE p.SerialNumber = :SerialNumber")
})

public class Printer extends AbstractEntity {;


    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "customer_id")
    @JsonBackReference
    private Customer customer;

    @ManyToOne
    @JoinColumn(name = "model_id")
    private PrinterModel PrinterModel;

    @Column
    private double bw_price_per_copy;


    @Column
    private double color_price_per_copy;

    @Column
    @Basic(optional = true)
    private String IdentificationOfPrinter;

    @Column
    @Basic(optional = false)
    private int NumOfBWCopy;

    @Column
    @Basic(optional = false)
    private int CURRENTNumOfBWCopy;

    @Column
    @Basic(optional = false)
    private int NumOfColorCopy;

    @Column
    @Basic(optional = false)
    private int CURRENTNumOfColorCopy;

    @Column
    @Basic(optional = false)
    private double rent_price;

    @Column(unique = true)
    @Basic(optional = false)
    private String SerialNumber;

    @Override
    public String toString() {
        return "Printer{\n" +
                "   Customer=   " + (customer != null ? "\n"+customer.toStringSimple()+'\n' : "\u001B[31mNo CUSTOMER\u001B[0m") + "\n" +
                "   model=" + (PrinterModel != null ? PrinterModel.getModelName() : "\u001B[31mNo MODEL\u001B[0m") + "\n" +
                "       bw_price_per_copy=" + bw_price_per_copy + "\n" +
                "       color_price_per_copy=" + color_price_per_copy + "\n" +
                "       IdentificationOfPrinter='" + IdentificationOfPrinter + "'\n" +
                "       NumOfBWCopy=" + NumOfBWCopy + "\n" +
                "       CURRENTNumOfBWCopy=" + CURRENTNumOfBWCopy + "\n" +
                "       NumOfColorCopy=" + NumOfColorCopy + "\n" +
                "       CURRENTNumOfColorCopy=" + CURRENTNumOfColorCopy + "\n" +
                "       rent_price=" + rent_price + "\n" +
                "       SerialNumber='" + SerialNumber + "'\n" +
                '}';
    }
    public String toStringSimple(){
        return "Printer{\n" +
                "   model=" + (PrinterModel != null ? PrinterModel.getModelName() : "\u001B[31mNo Model\u001B[0m") + "\n" +
                "   CURRENTNumOfBWCopy=" + CURRENTNumOfBWCopy + "\n" +
                "   SerialNumber='" + SerialNumber + "'\n" +
                '}';

    }


    /*
    * Tato funkce nemění paramety podléhající bidirektiví vyzbě @Customer @Model*/
    public void copyFrom(Printer printerToCopy) {
        Objects.requireNonNull(printerToCopy);
        this.bw_price_per_copy = printerToCopy.getBw_price_per_copy();
        this.color_price_per_copy = printerToCopy.getColor_price_per_copy();
        this.IdentificationOfPrinter = printerToCopy.getIdentificationOfPrinter();

        this.NumOfBWCopy = printerToCopy.getNumOfBWCopy();
        this.CURRENTNumOfBWCopy = printerToCopy.getCURRENTNumOfBWCopy();
        this.NumOfColorCopy = printerToCopy.getNumOfColorCopy();
        this.CURRENTNumOfColorCopy = printerToCopy.getCURRENTNumOfColorCopy();
        this.rent_price = printerToCopy.getRent_price();

    }
}
