package cz.cvut.printer_rental.model;

import jakarta.persistence.Embeddable;
import jakarta.persistence.Entity;
import lombok.Data;

@Embeddable
@Data
public class Dimensions {
    private double width;
    private double height;
    private double depth;


    public Dimensions() {}

    public Dimensions(double width, double height, double depth) {
        this.width = width;
        this.height = height;
        this.depth = depth;
    }

    public String toString() {
        return width + "x" + height + "x" + depth;
    }
}
