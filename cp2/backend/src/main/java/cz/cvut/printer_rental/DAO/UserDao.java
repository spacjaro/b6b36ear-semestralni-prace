package cz.cvut.printer_rental.dao;

import cz.cvut.printer_rental.model.Customer;
import cz.cvut.printer_rental.model.PhoneNumber;
import cz.cvut.printer_rental.model.User;
import jakarta.persistence.NoResultException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Repository;

import java.util.Objects;
import java.util.Optional;

@Repository
public class UserDao extends BaseDao<User> {
  public UserDao() {
    super(User.class);
  }

  // NamedQuery: Finds a user by username
  public User findByUsername(String username) {
    Objects.requireNonNull(username);

    return em.createNamedQuery("User.findByUsername", User.class)
        .setParameter("username", username)
        .getSingleResult();

  }

  // NamedQuery: Finds a user by email
  public User findByEmail(String email) {
    Objects.requireNonNull(email);

    return em.createNamedQuery("User.findByEmail", User.class)
        .setParameter("email", email)
        .getSingleResult();
  }

  // NamedQuery: Finds a user by phone number
  public User findByTel(PhoneNumber phoneNumber) {
    Objects.requireNonNull(phoneNumber);
    return em.createNamedQuery("User.findByTel", User.class)
        .setParameter("phoneNumber", phoneNumber)
        .getSingleResult();
  }

}
