package cz.cvut.printer_rental.service;

import cz.cvut.printer_rental.dao.BaseDao;
import cz.cvut.printer_rental.dao.QuarterYearDao;
import cz.cvut.printer_rental.exception.AlreadyExistsException;
import cz.cvut.printer_rental.exception.DuplicatePropertyException;
import cz.cvut.printer_rental.model.Month;
import cz.cvut.printer_rental.model.QuarterYear;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;

@Service
public class QuarterYearService extends AbstractCrudService<QuarterYear> {
  private static final Logger logger = LoggerFactory.getLogger(QuarterYearService.class);
  private final QuarterYearDao quarterYearDao;

  @Autowired
  public QuarterYearService(QuarterYearDao quarterYearDao) {
    this.quarterYearDao = quarterYearDao;
  }

  @Override
  @Transactional
  protected BaseDao<QuarterYear> selfDao() {
    return quarterYearDao;
  }

  @Override
  @Transactional
  public void save(QuarterYear entity) {
    try {
      checkUnique(entity);
      super.save(entity);
    } catch (DuplicatePropertyException e) {
      logger.error(e.getMessage());
      throw e;
    } catch (Exception e) {
      logger.error("Error occurred while saving entity: {}", entity, e);
      throw e;
    }
  }

  @Override
  @Transactional
  public QuarterYear findById(Integer id) {
    try {
      return super.findById(id);
    } catch (Exception e) {
      logger.error("Error occurred while finding entity by ID: {}", id, e);
      return null; // Returning null for methods that return an object
    }
  }

  @Override
  @Transactional
  public List<QuarterYear> findAll() {
    try {
      return super.findAll();
    } catch (Exception e) {
      logger.error("Error occurred while finding all entities", e);
      return null; // Returning null for methods that return a list
    }
  }

  @Override
  @Transactional
  public void update(QuarterYear entity) {
    try {
      checkUnique(entity);
      super.update(entity);
    } catch (DuplicatePropertyException e) {
      logger.error(e.getMessage());
      throw e;
    } catch (Exception e) {
      logger.error("Error occurred while updating entity: {}", entity, e);
      throw e;
    }
  }

  @Override
  @Transactional
  public void delete(QuarterYear entity) {
    try {
      super.delete(entity);
    } catch (Exception e) {
      logger.error("Error occurred while deleting entity: {}", entity, e);
      throw e; // Re-throwing the exception for void methods
    }
  }

  @Transactional
  public QuarterYear findByYearAndLastMonth(int year, int month) {
    try {
      Month m = Month.fromNumber(month);
      Objects.requireNonNull(m);
      return quarterYearDao.findByYearAndLastMonth(year, m);
    } catch (EmptyResultDataAccessException e) {
      logger.warn("No QuarterYear year: {} lastMonth: {} found", year, month);
      return null;
    } catch (Exception e) {
      logger.error("Error occurred while findByYearAndMonth: {} {}", year, month, e);
      return null;
    }
  }

  @Transactional
  public QuarterYear findByYearAndFirstMonth(int year, int month) {
    try {
      Month m = Month.fromNumber(month);
      Objects.requireNonNull(m);
      return quarterYearDao.findByYearAndFirstMonth(year, m);
    } catch (InvalidDataAccessApiUsageException e) {
      logger.warn("No result found findByYearAndFirstMonth: {} {}", year, month);
      return null;
    } catch (Exception e) {
      logger.error("Problem occurred while findByYearAndFirstMonth: {} {}", year, month);
      return null;
    }
  }

  @Transactional
  public QuarterYear getNextQuarterYear(QuarterYear quarterYear) {
    try {
      Objects.requireNonNull(quarterYear, "Input QuarterYear must not be null");

      int currentYear = quarterYear.getCurrentYear();
      Month lastMonth = quarterYear.getLastMonth();

      int nextStartMonthNumber = (lastMonth.getMonthNumber() + 2) % 12 + 1;

      int nextYear = currentYear + (nextStartMonthNumber < lastMonth.getMonthNumber() ? 1 : 0);

      // Vyhledáme další čtvrtletí v databázi
      QuarterYear next = findByYearAndLastMonth(nextYear, nextStartMonthNumber);
      if (next == null) {
        // Vytvoříme nové čtvrtletí, pokud neexistuje
        int firstMonthOfNextQuarter = (lastMonth.getMonthNumber() % 12) + 1;
        next = new QuarterYear(nextYear, firstMonthOfNextQuarter);
        quarterYearDao.persist(next);

        logger.debug("Created new QuarterYear: {}", next);
      }
      return next;
    } catch (Exception e) {
      logger.error("Error occurred while getting next quarter year", e);
      return null;
    }
  }

  @Transactional
  public QuarterYear getCurrentQuarterYear() {
    try {
      LocalDateTime nowDateTime = LocalDateTime.now();
      int currentYear = nowDateTime.getYear();
      int currentMonth = nowDateTime.getMonthValue();
      QuarterYear quarterYear = findByYearAndFirstMonth(currentYear, currentMonth);
      if (quarterYear == null) {
        logger.warn("No QuarterYear found for year: {} and lastMonth: {}. Creating NEW", currentYear, currentMonth);
        QuarterYear newQuarterYear = new QuarterYear(currentYear, currentMonth);
        save(newQuarterYear);
        return newQuarterYear;
      } else {
        return quarterYear;
      }
    } catch (Exception e) {
      logger.error("Error occurred while getting current quarter year : {}", e.getMessage());
      throw e;
    }
  }

  @Transactional(readOnly = true)
  protected void checkUnique(QuarterYear entity) {
    List<QuarterYear> quarterYears = quarterYearDao.findAll(); // Získáme všechny existující záznamy

    // Pokud existuje alespoň jeden záznam se stejnými hodnotami, vyhodíme výjimku
    boolean isDuplicate = quarterYears.stream()
        .anyMatch(q -> q.getCurrentYear() == entity.getCurrentYear() &&
            q.getFirstMonth() == entity.getFirstMonth() &&
            q.getLastMonth() == entity.getLastMonth());

    if (isDuplicate) {
      throw new DuplicatePropertyException(
          String.format("QuarterYear with year %d, firstMonth %s, and lastMonth %s already exists.",
              entity.getCurrentYear(),
              entity.getFirstMonth(),
              entity.getLastMonth()));
    }
  }

}
