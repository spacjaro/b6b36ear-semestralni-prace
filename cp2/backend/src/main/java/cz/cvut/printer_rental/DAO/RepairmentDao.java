package cz.cvut.printer_rental.dao;

import cz.cvut.printer_rental.model.RepairItem;
import cz.cvut.printer_rental.model.Repairment;
import org.springframework.stereotype.Repository;

@Repository
public class RepairmentDao extends BaseDao<Repairment> {
  public RepairmentDao() {
    super(Repairment.class);
  }
}
