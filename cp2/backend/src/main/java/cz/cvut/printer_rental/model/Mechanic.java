package cz.cvut.printer_rental.model;

import jakarta.persistence.*;

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name="userType", discriminatorType = DiscriminatorType.INTEGER)
@Access(AccessType.FIELD)
public class Mechanic extends User {
    /**
     * Constant representing the rights of a mechanic user.
     */
    public static final Role role = Role.MECHANIC;

    /**
     * Retrieves the rights of the mechanic user.
     *
     * @return Rights of the mechanic user.
     */
    @Override
    public Role getRole() {
        return role;
    }

    /**
     * Default constructor for the Mechanic class.
     * Constructs a mechanic user with default settings.
     */
    public Mechanic() {
        super();
    }

    /**
     * Constructor for the Mechanic class that initializes it based on another User object.
     *
     * @param u Another User object whose properties are used to initialize this Mechanic object.
     */
    public Mechanic(User u) {
        super(u);
    }
}
