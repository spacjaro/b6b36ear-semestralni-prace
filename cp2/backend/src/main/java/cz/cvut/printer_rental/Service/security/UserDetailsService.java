package cz.cvut.printer_rental.service.security;

import cz.cvut.printer_rental.dao.UserDao;
import cz.cvut.printer_rental.service.PrinterService;
import cz.cvut.printer_rental.model.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Objects;
import java.util.Optional;

@Service
public class UserDetailsService implements org.springframework.security.core.userdetails.UserDetailsService {

  private static final Logger logger = LoggerFactory.getLogger(UserDetailsService.class);
  private final UserDao dao;

  @Autowired
  public UserDetailsService(UserDao dao) {
    this.dao = dao;
  }

  @Override
  public UserDetails loadUserByUsername(String username) {
    Objects.requireNonNull(username);
    try {
      User u = dao.findByUsername(username);
      return new cz.cvut.printer_rental.security.UserDetails(u);
    } catch (Exception e) {
      logger.warn(e.getMessage());
      return null;
    }
  }

}
