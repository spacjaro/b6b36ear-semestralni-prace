package cz.cvut.printer_rental.security;

import com.fasterxml.jackson.databind.ObjectMapper;
import cz.cvut.printer_rental.utils.Constants;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;

import java.io.IOException;

/**
 * Returns info about authentication failure.
 *
 * Differs from default implementation in that it returns a custom JSON
 * response.
 */
public class AuthenticationFailure implements AuthenticationFailureHandler {

  private static final Logger logger = LoggerFactory.getLogger(AuthenticationFailure.class);

  private final ObjectMapper mapper;

  public AuthenticationFailure(ObjectMapper mapper) {
    this.mapper = mapper;
  }

  @Override
  public void onAuthenticationFailure(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse,
      AuthenticationException e) throws IOException {
    logger.debug("Login failed for user {}.", httpServletRequest.getParameter(Constants.USERNAME_PARAM));
    final LoginStatus status = new LoginStatus(false, false, null, e.getMessage());
    // Přesměrování na přihlašovací stránku s parametrem 'error=true'
    httpServletResponse.sendRedirect("/login?error=true");
    /*
     * AJAX tady nikdo nechce
     */
    // mapper.writeValue(httpServletResponse.getOutputStream(), status);
  }
}
