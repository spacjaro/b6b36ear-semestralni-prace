package cz.cvut.printer_rental.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import cz.cvut.printer_rental.exception.AlreadyPresentException;
import cz.cvut.printer_rental.exception.NotAvailableException;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.*;
import java.util.stream.Collectors;

@EqualsAndHashCode(callSuper = true)
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Access(AccessType.FIELD)
@Data

@NamedQueries({
    @NamedQuery(name = "Customer.findByCompanyName", query = "SELECT c FROM Customer c WHERE c.companyName = :companyName"),
    @NamedQuery(name = "Customer.findByFirstName", query = "SELECT c FROM Customer c WHERE c.firstName = :firstName"),
    @NamedQuery(name = "Customer.findByLastName", query = "SELECT c FROM Customer c WHERE c.lastName = :lastName"),
    @NamedQuery(name = "Customer.findByQuarterYear", query = "SELECT DISTINCT i.customer FROM Invoice i WHERE i.quarterYear = :quarterYear"),

    @NamedQuery(name = "Customer.findByICO", query = "SELECT c FROM Customer c WHERE c.ico = :ico"),
    @NamedQuery(name = "Customer.findByDIC", query = "SELECT c FROM Customer c WHERE c.dic = :dic"),
    @NamedQuery(name = "Customer.findByEmail", query = "SELECT c FROM Customer c WHERE c.email = :email"),
    @NamedQuery(name = "Customer.findByPhoneNumber", query = "SELECT c FROM Customer c WHERE c.phoneNumber = :phoneNumber"),
})

public class Customer extends AbstractEntity {

  @Column(name = "company", unique = true)
  private String companyName;

  @Column(name = "firstname")
  private String firstName;

  @Column(name = "lastname")
  private String lastName;

  @Column(name = "ICO", unique = true)
  private String ico;

  @Column(name = "DIC", unique = true)
  private String dic;

  @Embedded
  @Basic(optional = false)
  private Address address;

  @Basic(optional = false)
  @Column(name = "email", nullable = false, unique = true)
  private String email;

  @Embedded
  @Basic(optional = false)
  @Column(unique = true)
  private PhoneNumber phoneNumber;

  @OneToMany(mappedBy = "customer")
  @JsonIgnore
  private List<Printer> printers;

  public void addPrinterToCustomer(Printer printer) {
    Objects.requireNonNull(printer, "Printer cannot be null");
    if (printers == null) {
      printers = new ArrayList<>();
    }
    final Optional<Printer> existing = printers.stream().filter(p -> p.getId().equals(printer.getId())).findAny();
    if (existing.isPresent()) {
      throw new AlreadyPresentException(
          String.format("Customer with ID %d already has printer: %s", this.getId(), printer.toStringSimple()));
    } else {
      printers.add(printer);
    }
  }

  public void removePrinterFromCustomer(Printer printer) {
    Objects.requireNonNull(printer, "Printer cannot be null");
    if (printers == null) {
      throw new NotAvailableException(String.format("Customer with ID %d has no printers", this.getId()));
    }
    final Optional<Printer> existing = printers.stream().filter(p -> p.getId().equals(printer.getId())).findAny();
    if (existing.isPresent()) {
      final Iterator<Printer> iterator = printers.iterator();
      while (iterator.hasNext()) {
        final Printer currentPrinter = iterator.next();
        if (currentPrinter.getId().equals(printer.getId())) {
          iterator.remove();
          break;
        }
      }
    } else {
      throw new NotAvailableException(
          String.format("Customer with ID %d has no printer: %s", this.getId(), printer.toStringSimple()));
    }
  }

  public void copyFrom(Customer newCustomer) {
    Objects.requireNonNull(newCustomer);
    this.companyName = newCustomer.getCompanyName();
    this.firstName = newCustomer.getFirstName();
    this.lastName = newCustomer.getLastName();
    this.ico = newCustomer.getIco();
    this.dic = newCustomer.getDic();
    this.address = newCustomer.getAddress();
    this.email = newCustomer.getEmail();
    this.phoneNumber = newCustomer.getPhoneNumber();
  }

  @Override
  public String toString() {
    return "Customer{\n" +
        "   companyName='" + companyName + "'\n" +
        "   firstName='" + firstName + "'\n" +
        "   lastName='" + lastName + "'\n" +
        "   ico='" + ico + "'\n" +
        "   DIC='" + dic + "'\n" +
        "   address=" + address + "\n" +
        "   email='" + email + "'\n" +
        "   phoneNumber=" + phoneNumber + "\n" +
        "   printers= " + (printers != null ? "\n" + printers.stream()
            .map(printer -> "      " + printer.toStringSimple())
            .collect(Collectors.joining("\n")) : "\u001B[31m<null>\u001B[0m")
        + "\n" +
        "}";
  }

  public String toStringSimple() {
    return "   companyName='" + companyName + "'\n" +
        "   firstName='" + firstName + "'\n" +
        "   lastName='" + lastName + "'\n" +
        "   printers= " + (printers != null ? printers.size() : "\u001B[31m<null>\u001B[0m") + "\n";
  }
}
