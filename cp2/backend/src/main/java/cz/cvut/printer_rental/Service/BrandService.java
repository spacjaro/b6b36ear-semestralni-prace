package cz.cvut.printer_rental.service;

import cz.cvut.printer_rental.dao.BaseDao;
import cz.cvut.printer_rental.dao.BrandDao;
import cz.cvut.printer_rental.dao.ModelDao;
import cz.cvut.printer_rental.exception.DuplicatePropertyException;
import cz.cvut.printer_rental.model.Brand;
import cz.cvut.printer_rental.model.Customer;
import cz.cvut.printer_rental.model.Model;
import cz.cvut.printer_rental.model.Printer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Objects;

@Service
public class BrandService extends AbstractCrudService<Brand> {
  private static final Logger logger = LoggerFactory.getLogger(BrandService.class);

  private final BrandDao brandDao;

  @Autowired
  public BrandService(BrandDao brandDao) {
    this.brandDao = brandDao;
  }

  @Override
  protected BaseDao<Brand> selfDao() {
    return brandDao;
  }

  @Override
  @Transactional
  public void save(Brand entity) {
    Objects.requireNonNull(entity, "Brand entity cannot be null");
    try {
      checkForDuplicates(entity);
      super.save(entity);
    } catch (Exception e) {
      logger.error(e.getMessage());
      throw e;
    }
  }

  @Override
  @Transactional
  public void update(Brand entity) {
    Objects.requireNonNull(entity, "Brand entity cannot be null");
    try {
      checkForDuplicates(entity);
      super.update(entity);
    } catch (Exception e) {
      logger.error(e.getMessage());
      throw e;
    }
  }

  @Transactional
  public void deleteById(Integer id) {
    try {
      Brand brandToRemove = brandDao.find(id);
      super.delete(brandToRemove);
    } catch (Exception e) {
      logger.error(e.getMessage());
      throw e;
    }
  }

  private void checkForDuplicates(Brand brand) {
    Objects.requireNonNull(brand, "Brand entity cannot be null");

    // Najdeme existující značku podle názvu
    Brand existingBrand = brandDao.findByName(brand.getBrandName());
    if (existingBrand != null && !existingBrand.getId().equals(brand.getId())) {
      throw new DuplicatePropertyException("Brand with name " + brand.getBrandName() + " already exists.");
    }
  }
}
