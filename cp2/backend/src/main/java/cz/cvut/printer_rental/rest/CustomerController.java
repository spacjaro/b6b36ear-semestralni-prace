package cz.cvut.printer_rental.rest;

import cz.cvut.printer_rental.rest.util.RestUtils;
import cz.cvut.printer_rental.rest.util.CustomerUtils;
import cz.cvut.printer_rental.exception.DuplicatePropertyException;
import cz.cvut.printer_rental.service.CustomerService;
import cz.cvut.printer_rental.model.Customer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import jakarta.persistence.EntityNotFoundException;
import jakarta.persistence.PersistenceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

@RestController
@RequestMapping("/api/v1/customers")
public class CustomerController {
  private static final Logger logger = LoggerFactory.getLogger(CustomerController.class);

  private final CustomerService customerService;

  @Autowired
  public CustomerController(CustomerService customerService) {
    this.customerService = customerService;
  }

  @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<List<Customer>> getAllCustomers(
      @RequestParam(required = false) String companyName,
      @RequestParam(required = false) String firstName,
      @RequestParam(required = false) String lastName,
      @RequestParam(required = false) String ico,
      @RequestParam(required = false) String dic,
      @RequestParam(required = false) String email
  // TODO: Add phone number param
  ) {

    List<Customer> customers = Collections.emptyList();

    // TODO: More sophisticated where it is possible to have more than 1 query
    // params
    if (companyName != null) {
      customers = CustomerUtils.getCustomerList(customerService.getCustomerByCompanyName(companyName));
    } else if (firstName != null) {
      customers = customerService.getCustomersByFirstName(firstName);
    } else if (lastName != null) {
      customers = customerService.getCustomersByLastName(lastName);
    } else if (ico != null) {
      customers = CustomerUtils.getCustomerList(customerService.getCustomerByICO(ico));
    } else if (dic != null) {
      customers = CustomerUtils.getCustomerList(customerService.getCustomerByDIC(dic));
    } else if (email != null) {
      customers = CustomerUtils.getCustomerList(customerService.getCustomerByEmail(email));
    } else {
      customers = customerService.findAll();
    }

    return ResponseEntity.ok(customers);
  }

  @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<Customer> getCustomer(@PathVariable Integer id) {
    Customer customer = customerService.findById(id);
    if (customer == null) {
      logger.warn("Customer not found with id {}", id);
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
    return ResponseEntity.ok(customer);
  }

  // Create
  @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<Customer> createCustomer(@RequestBody Customer customer) {
    Customer createdCustomer = customer; // TODO: Return created customer from Service layer
    customerService.save(createdCustomer);
    logger.debug("Customer created: {}", createdCustomer);
    return new ResponseEntity<>(createdCustomer, HttpStatus.CREATED);
  }

  // Update
  @PutMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<Customer> updateCustomer(@PathVariable Integer id, @RequestBody Customer customer) {
    customer.setId(id); // Set the customer ID from the URL to the customer object
    Customer updatedCustomer = customer; // TODO: Return updated customer from Service layer
    customerService.update(updatedCustomer);
    logger.debug("Customer updated: {}", updatedCustomer);
    return new ResponseEntity<>(updatedCustomer, HttpStatus.OK);
  }

  // Delete
  @DeleteMapping(path = "/{id}")
  public ResponseEntity<Void> deleteCustomer(@PathVariable Integer id) {
    customerService.deleteById(id);
    logger.debug("Customer deleted with id: {}", id);
    return new ResponseEntity<>(HttpStatus.NO_CONTENT);
  }

  // Custom
  @PostMapping(path = "/assign-printer/{customerID}/{printerID}")
  public ResponseEntity<Void> assignPrinterToCustomer(@PathVariable Integer customerID,
      @PathVariable Integer printerID) {
    customerService.assignPrinterToCustomer(customerID, printerID);
    return new ResponseEntity<>(HttpStatus.CREATED);
  }

  @PostMapping(path = "/remove-printer/{customerID}/{printerID}")
  public ResponseEntity<Void> removePrinterFromCustomer(@PathVariable Integer customerID,
      @PathVariable Integer printerID) {
    customerService.removePrinterFromCustomer(customerID, printerID);
    return new ResponseEntity<>(HttpStatus.NO_CONTENT);
  }
}