package cz.cvut.printer_rental.model;

import jakarta.persistence.*;

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name="userType", discriminatorType = DiscriminatorType.INTEGER)
@Access(AccessType.FIELD)
public class SuperAdmin extends User{
    private static final Role role =Role.SUPER_ADMIN;

    public SuperAdmin(User u) {
        super(u);
    }

    @Override
    public Role getRole() {return role;}
    public SuperAdmin() {super();}
}
