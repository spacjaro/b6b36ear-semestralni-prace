package cz.cvut.printer_rental.service;

import cz.cvut.printer_rental.dao.BaseDao;
import cz.cvut.printer_rental.dao.CustomerDao;
import cz.cvut.printer_rental.dao.PrinterDao;
import cz.cvut.printer_rental.exception.DuplicatePropertyException;
import cz.cvut.printer_rental.exception.InvalidInputException;
import cz.cvut.printer_rental.model.Customer;
import cz.cvut.printer_rental.model.Printer;
import cz.cvut.printer_rental.model.PrinterModel;
import jakarta.transaction.Transactional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

@Service
public class PrinterService extends AbstractCrudService<Printer> {
  private static final Logger logger = LoggerFactory.getLogger(PrinterService.class);

  private final PrinterDao printerDao;
  private final CustomerDao customerDao;

  private final ModelService modelService;

  @Autowired
  public PrinterService(PrinterDao printerDao, CustomerDao customerDao, ModelService modelService) {
    this.customerDao = customerDao;
    this.printerDao = printerDao;
    this.modelService = modelService;
  }

  @Override
  protected BaseDao<Printer> selfDao() {
    return printerDao;
  }

  @Override
  @Transactional
  public void save(Printer entity) {
    Objects.requireNonNull(entity, "Printer cannot be null");
    checkForDuplicates(entity);
    super.save(entity);
  }

  @Override
  @Transactional
  public void update(Printer entity) {
    Objects.requireNonNull(entity, "Printer cannot be null");
    checkForDuplicates(entity);
    if (printerDao.exists(entity.getId())) {
      Printer existing = printerDao.find(entity.getId());
      existing.copyFrom(entity);
      printerDao.update(existing);
      return;
    }
    super.update(entity);
  }

  @Transactional
  public void deleteById(Integer id) {
    Printer printer = printerDao.find(id);
    delete(printer);
  }

  @Transactional
  public List<Printer> findByCustomer(Integer customerId) {
    Customer customer = customerDao.find(customerId);
    if (customer == null) {
      logger.error("Customer with ID {} not found", customerId);
      return null;
    }
    return printerDao.findByCustomer(customer);
  }

  @Transactional
  public List<Printer> findPrintersByModel(Integer modelId) {
    PrinterModel model = modelService.getPrinterModelByID(modelId);
    if (model == null) {
      logger.error("Model with ID {} not found", modelId);
      return null;
    }
    return printerDao.findByModel(model);
  }

  @Transactional
  public List<Printer> findPrintersWithNoCustomer() {
    return printerDao.findFree();
  }

  @Transactional
  public void AssignCustomerToPrinter(Customer customer, Printer printer) {
    Objects.requireNonNull(customer);
    Objects.requireNonNull(printer);
    printer.setCustomer(customer);
    printerDao.update(printer);
    logger.debug("Successfully assigned customer {} to printer {}", customer.getId(), printer.getId());
  }

  @Transactional
  public void unassignCustomerFromPrinter(Printer printer) {
    printer.setCustomer(null);
    printerDao.update(printer);
  }

  @Transactional
  public void EstablishCounts(Printer printer) {
    Objects.requireNonNull(printer);
    printer.setNumOfBWCopy(printer.getCURRENTNumOfBWCopy());
    printer.setNumOfColorCopy(printer.getCURRENTNumOfColorCopy());
    printerDao.update(printer);
  }

  @Transactional
  public void SetCURRENTNumOfBWCopy(Integer printerId, Integer amount) {
    Printer printer = printerDao.find(printerId);
    if (printer.getCURRENTNumOfBWCopy() < amount) {
      printer.setCURRENTNumOfBWCopy(amount);
      printerDao.update(printer);
    } else {
      logger.warn("Invalid input current amount of BWCopy {} is higher than amount {}",
          printer.getCURRENTNumOfBWCopy(), amount);
      throw new InvalidInputException(
          String.format("Invalid input: current amount %d is higher than amount %d",
              printer.getCURRENTNumOfBWCopy(), amount));
    }
  }

  @Transactional
  public void SetCURRENTNumOfColorCopy(Integer printerId, Integer amount) {
    Printer printer = printerDao.find(printerId);
    if (printer.getCURRENTNumOfColorCopy() < amount) {
      printer.setCURRENTNumOfColorCopy(amount);
      printerDao.update(printer);
    } else {
      logger.warn("Invalid input current amount of ColorCopy{} is higher than amount {}",
          printer.getCURRENTNumOfColorCopy(), amount);
      throw new InvalidInputException(
          String.format("Invalid input: current amount %d is higher than amount %d",
              printer.getCURRENTNumOfColorCopy(), amount));
    }
  }

  private void checkForDuplicates(Printer entity) {
    Objects.requireNonNull(entity);
    Printer existingPrinter = printerDao.findBySerialNumber(entity.getSerialNumber());
    if (existingPrinter != null && !existingPrinter.getId().equals(entity.getId())) {
      throw new DuplicatePropertyException(
          "Printer with serial number " + entity.getSerialNumber() + " already exists.");
    }
  }
}
