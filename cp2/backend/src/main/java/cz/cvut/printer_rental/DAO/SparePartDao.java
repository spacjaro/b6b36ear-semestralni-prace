package cz.cvut.printer_rental.dao;

import cz.cvut.printer_rental.model.Model;
import cz.cvut.printer_rental.model.PrinterModel;
import cz.cvut.printer_rental.model.SparePart;
import cz.cvut.printer_rental.model.SparePartModel;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Objects;

@Repository
public class SparePartDao extends BaseDao<SparePart> {
  public SparePartDao() {
    super(SparePart.class);
  }

  // NamedQuery: Finds all spare parts for a specific printer model
  public List<SparePart> findCompatibleForPrinterModel(PrinterModel printerModel) {
    Objects.requireNonNull(printerModel);
    return em.createNamedQuery("SparePart.findCompatibleForPrinterModel", SparePart.class)
        .setParameter("PrinterModel", printerModel)
        .getResultList();

  }

  // NamedQuery: Finds all spare parts for a specific spare part model
  public List<SparePart> findByModel(SparePartModel model) {
    Objects.requireNonNull(model);
    return em.createNamedQuery("SparePart.findByModel", SparePart.class)
        .setParameter("model", model)
        .getResultList();
  }

}
