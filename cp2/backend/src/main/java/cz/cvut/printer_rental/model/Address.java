package cz.cvut.printer_rental.model;

import jakarta.persistence.Embeddable;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import java.util.Objects;

/**
 * Represents an address with street, city, postal code, and country.
 * This class is embeddable in other entities.
 * 
 * <hr />
 * Fields:
 * <ul>
 * <li>{@code street} - The street address.</li>
 * <li>{@code city} - The city of the address.</li>
 * <li>{@code postalCode} - The postal code of the address.</li>
 * <li>{@code country} - The country of the address.</li>
 * </ul>
 */
@Getter
@Setter
@Embeddable
@NoArgsConstructor
@AllArgsConstructor
public class Address {

  private String street;
  private String city;
  private String postalCode;
  private String country;

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Address address = (Address) o;
    return Objects.equals(street, address.street) &&
        Objects.equals(city, address.city) &&
        Objects.equals(postalCode, address.postalCode) &&
        Objects.equals(country, address.country);
  }

  @Override
  public int hashCode() {
    return Objects.hash(street, city, postalCode, country);
  }

  @Override
  public String toString() {
    return " " + street + " " + city + " " + postalCode + " " + country;
  }
}
