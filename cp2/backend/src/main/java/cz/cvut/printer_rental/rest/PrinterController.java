package cz.cvut.printer_rental.rest;

import cz.cvut.printer_rental.model.Printer;
import cz.cvut.printer_rental.service.PrinterService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/printers")
public class PrinterController {
  private static final Logger logger = LoggerFactory.getLogger(PrinterController.class);

  private final PrinterService printerService;

  @Autowired
  public PrinterController(PrinterService printerService) {
    this.printerService = printerService;
  }

  // Get all printers
  @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<List<Printer>> getAllPrinters() {
    List<Printer> printers = printerService.findAll();
    if (printers.isEmpty()) {
      logger.warn("No printers found");
      return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
    return ResponseEntity.ok(printers);
  }

  // Get printer by ID
  @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<Printer> getPrinter(@PathVariable Integer id) {
    Printer printer = printerService.findById(id);
    if (printer == null) {
      logger.warn("Printer not found with id {}", id);
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
    return ResponseEntity.ok(printer);
  }

  // Create a new printer
  @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<Printer> createPrinter(@RequestBody Printer printer) {
    printerService.save(printer);
    logger.debug("Printer created: {}", printer);
    return new ResponseEntity<>(printer, HttpStatus.CREATED);
  }

  // Update an existing printer
  @PutMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<Printer> updatePrinter(@PathVariable Integer id, @RequestBody Printer printer) {
    printer.setId(id);
    printerService.update(printer);
    logger.debug("Printer updated: {}", printer);
    return new ResponseEntity<>(printer, HttpStatus.OK);
  }

  // Delete a printer
  @DeleteMapping(path = "/{id}")
  public ResponseEntity<Void> deletePrinter(@PathVariable Integer id) {
    printerService.deleteById(id);
    logger.debug("Printer deleted with id: {}", id);
    return new ResponseEntity<>(HttpStatus.NO_CONTENT);
  }
}