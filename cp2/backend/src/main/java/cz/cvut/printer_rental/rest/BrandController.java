package cz.cvut.printer_rental.rest;

import cz.cvut.printer_rental.model.Brand;
import cz.cvut.printer_rental.service.BrandService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/brands")
public class BrandController {
  private static final Logger logger = LoggerFactory.getLogger(BrandController.class);

  private final BrandService brandService;

  @Autowired
  public BrandController(BrandService brandService) {
    this.brandService = brandService;
  }

  // Get all brands
  @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<List<Brand>> getAllBrands() {
    List<Brand> brands = brandService.findAll();
    if (brands.isEmpty()) {
      logger.warn("No brands found");
      return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
    return ResponseEntity.ok(brands);
  }

  // Get brand by ID
  @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<Brand> getBrand(@PathVariable Integer id) {
    Brand brand = brandService.findById(id);
    if (brand == null) {
      logger.warn("Brand not found with id {}", id);
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
    return ResponseEntity.ok(brand);
  }

  // Create a new brand
  @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<Brand> createBrand(@RequestBody Brand brand) {
    brandService.save(brand);
    logger.debug("Brand created: {}", brand);
    return new ResponseEntity<>(brand, HttpStatus.CREATED);
  }

  // Update an existing brand
  @PutMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<Brand> updateBrand(@PathVariable Integer id, @RequestBody Brand brand) {
    brand.setId(id);
    brandService.update(brand);
    logger.debug("Brand updated: {}", brand);
    return new ResponseEntity<>(brand, HttpStatus.OK);
  }

  // Delete a brand
  @DeleteMapping(path = "/{id}")
  public ResponseEntity<Void> deleteBrand(@PathVariable Integer id) {
    brandService.deleteById(id);
    logger.debug("Brand deleted with id: {}", id);
    return new ResponseEntity<>(HttpStatus.NO_CONTENT);
  }
}
