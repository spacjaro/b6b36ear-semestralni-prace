package cz.cvut.printer_rental.dao;

import cz.cvut.printer_rental.model.Customer;
import cz.cvut.printer_rental.model.Invoice;
import cz.cvut.printer_rental.model.QuarterYear;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Objects;

@Repository
public class InvoiceDao extends BaseDao<Invoice> {
  public InvoiceDao() {
    super(Invoice.class);
  }

  /**
   * Finds all invoices for the specified customer.
   *
   * @param customer the customer whose invoices are to be retrieved.
   * @return a list of invoices for the given customer, or null if no invoices are
   *         found or in case of an error.
   */
  public List<Invoice> findByCustomer(Customer customer) {
    Objects.requireNonNull(customer);
    return em.createNamedQuery("Invoice.findByCustomer", Invoice.class)
        .setParameter("customer", customer)
        .getResultList();
  }

  /**
   * Finds all invoices for the specified quarter and year.
   *
   * @param quarterYear the quarter and year for which invoices are to be
   *                    retrieved.
   * @return a list of invoices for the given quarter and year, or null if no
   *         invoices are found or in case of an error.
   */
  public List<Invoice> findByQuarterYear(QuarterYear quarterYear) {
    Objects.requireNonNull(quarterYear);
    return em.createNamedQuery("Invoice.findByQuarterYear", Invoice.class)
        .setParameter("quarterYear", quarterYear)
        .getResultList();
  }

  /**
   * Finds all unpaid invoices.
   *
   * @return a list of unpaid invoices, or null if no unpaid invoices are found or
   *         in case of an error.
   */
  public List<Invoice> findUnpaid() {
    return em.createNamedQuery("Invoice.findUnpaid", Invoice.class)
        .getResultList();
  }

  /**
   * Finds all paid invoices.
   *
   * @return a list of paid invoices, or null if no paid invoices are found or in
   *         case of an error.
   */
  public List<Invoice> findPaid() {
    return em.createNamedQuery("Invoice.findPaid", Invoice.class)
        .getResultList();
  }

}
