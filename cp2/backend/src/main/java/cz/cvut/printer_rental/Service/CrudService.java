package cz.cvut.printer_rental.service;

import java.util.List;

/**
 * Generic interface for CRUD operations.
 *
 * @param <T> the type of the entity
 */
public interface CrudService<T> {

  /**
   * Saves the given entity.
   *
   * @param entity the entity to save
   */
  void save(T entity);

  /**
   * Finds an entity by its ID.
   *
   * @param id the ID of the entity
   * @return the found entity, or null if not found
   */
  T findById(Integer id);

  /**
   * Finds all entities.
   *
   * @return a list of all entities
   */
  List<T> findAll();

  /**
   * Updates the given entity.
   *
   * @param entity the entity to update
   */
  void update(T entity);

  /**
   * Deletes the given entity.
   *
   * @param entity the entity to delete
   */
  void delete(T entity);
}