package cz.cvut.printer_rental.exception;

public class DuplicatePropertyException extends RuntimeException {
  public DuplicatePropertyException(String message) {
    super(message);
  }
}
