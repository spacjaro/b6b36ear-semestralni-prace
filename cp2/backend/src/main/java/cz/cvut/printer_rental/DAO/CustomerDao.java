package cz.cvut.printer_rental.dao;

import cz.cvut.printer_rental.model.Customer;
import cz.cvut.printer_rental.model.PhoneNumber;
import cz.cvut.printer_rental.model.QuarterYear;
import jakarta.persistence.NoResultException;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Objects;

@Repository
public class CustomerDao extends BaseDao<Customer> {
  protected CustomerDao() {
    super(Customer.class);
  }

  // NamedQuery: Find Customer by CompanyName
  /**
   * Finds a customer by the company name.
   *
   * @param companyName the name of the company to search for.
   * @return the Customer with the given company name, or null if no customer is
   *         found.
   */
  public Customer findByCompanyName(String companyName) {
    Objects.requireNonNull(companyName);

    return em.createNamedQuery("Customer.findByCompanyName", Customer.class)
        .setParameter("companyName", companyName)
        .getSingleResult();

  }

  // NamedQuery: Find Customers by FirstName
  /**
   * Finds customers by their first name.
   *
   * @param firstName the first name of the customers to search for.
   * @return a list of customers with the given first name, or null if no
   *         customers are found.
   */
  public List<Customer> findByFirstName(String firstName) {
    Objects.requireNonNull(firstName);

    return em.createNamedQuery("Customer.findByFirstName", Customer.class)
        .setParameter("firstName", firstName)
        .getResultList();

  }

  // NamedQuery: Find Customers by LastName
  /**
   * Finds customers by their last name.
   *
   * @param lastName the last name of the customers to search for.
   * @return a list of customers with the given last name, or null if no customers
   *         are found.
   */
  public List<Customer> findByLastName(String lastName) {
    Objects.requireNonNull(lastName);

    return em.createNamedQuery("Customer.findByLastName", Customer.class)
        .setParameter("lastName", lastName)
        .getResultList();
  }

  // NamedQuery: Find Customers by QuarterYear
  /**
   * Finds customers by their quarter and year.
   *
   * @param quarterYear the quarter and year of the customers to search for.
   * @return a list of customers in the specified quarter and year, or null if no
   *         customers are found.
   */
  public List<Customer> findByQuarterYear(QuarterYear quarterYear) {
    Objects.requireNonNull(quarterYear);
    List<Customer> resultList = em.createNamedQuery("Customer.findByQuarterYear", Customer.class)
        .setParameter("quarterYear", quarterYear)
        .getResultList();
    return resultList.isEmpty() ? null : resultList; // Return null if the list is empty
  }

  // NamedQuery: Find Customer by ICO
  /**
   * Finds a customer by their ICO (Identification Number).
   *
   * @param ICO the ICO to search for.
   * @return the Customer with the given ICO, or null if no customer is found.
   */
  public Customer findByICO(String ICO) {
    Objects.requireNonNull(ICO);
    return em.createNamedQuery("Customer.findByICO", Customer.class)
        .setParameter("ico", ICO)
        .getSingleResult();
  }

  // NamedQuery: Find Customer by DIC
  /**
   * Finds a customer by their DIC (Tax Identification Number).
   *
   * @param DIC the DIC to search for.
   * @return the Customer with the given DIC, or null if no customer is found.
   */
  public Customer findByDIC(String DIC) {
    Objects.requireNonNull(DIC);
    return em.createNamedQuery("Customer.findByDIC", Customer.class)
        .setParameter("dic", DIC)
        .getSingleResult();
  }

  // NamedQuery: Find Customer by Email
  /**
   * Finds a customer by their email address.
   *
   * @param email the email to search for.
   * @return the Customer with the given email, or null if no customer is found.
   */
  public Customer findByEmail(String email) {
    Objects.requireNonNull(email);
    return em.createNamedQuery("Customer.findByEmail", Customer.class)
        .setParameter("email", email)
        .getSingleResult();
  }

  // NamedQuery: Find Customer by PhoneNumber
  /**
   * Finds a customer by their phone number.
   *
   * @param phoneNumber the phone number to search for.
   * @return the Customer with the given phone number, or null if no customer is
   *         found.
   */
  public Customer findByPhoneNumber(PhoneNumber phoneNumber) {
    Objects.requireNonNull(phoneNumber);
    return em.createNamedQuery("Customer.findByPhoneNumber", Customer.class)
        .setParameter("phoneNumber", phoneNumber)
        .getSingleResult();
  }
}
