package cz.cvut.printer_rental.model;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name="modelType", discriminatorType = DiscriminatorType.STRING)
@Access(AccessType.FIELD)
public class SparePartModel extends Model{

    @ManyToMany
    @JoinTable(
            name = "SPAREPART_MODEL_TO_PRINTER_MODEL",
            joinColumns = @JoinColumn(name = "spare_part_model_id"),
            inverseJoinColumns = @JoinColumn(name = "printer_model_id")
    )
    private List<PrinterModel> CompatibleWithModels;

    @Override
    public String toString() {
        return "PrinterModel{" + "\n"+
                "  "+super.getBrand().getBrandName() + "\n" +
                "  "+super.getModelName() + "\n" +
                "  "+super.getEAN() + "\n" +
                "    CompatibleWith "+ (CompatibleWithModels!=null?CompatibleWithModels.size():"\u001B[31m<null>\u001B[0m") +"\n" +
                '}';
    }

    public void addPrinterModel(PrinterModel printerModel) {
        if (CompatibleWithModels==null) {
            CompatibleWithModels = new ArrayList<PrinterModel>();
        }
        if (!CompatibleWithModels.contains(printerModel)) {
            CompatibleWithModels.add(printerModel);
        }
    }
}
