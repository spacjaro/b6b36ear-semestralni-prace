package cz.cvut.printer_rental.service;

import cz.cvut.printer_rental.dao.*;
import cz.cvut.printer_rental.exception.AlreadyExistsException;
import cz.cvut.printer_rental.model.*;
import jakarta.persistence.Access;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
public class ServiceRecordService extends AbstractCrudService<ServiceRecord> {
  private static final Logger logger = LoggerFactory.getLogger(ServiceRecordService.class);
  private final PrinterDao printerDao;
  private final InvoiceService invoiceService;
  private final ServiceRecordDao serviceRecordDao;
  private final RepairmentService repairmentService;

  @Autowired
  public ServiceRecordService(PrinterDao printerDao, InvoiceDao invoiceDao, InvoiceService invoiceService,
      ServiceRecordDao serviceRecordDao, RepairmentService repairmentService) {
    this.printerDao = printerDao;
    this.invoiceService = invoiceService;
    this.serviceRecordDao = serviceRecordDao;
    this.repairmentService = repairmentService;
  }

  @Override
  protected BaseDao<ServiceRecord> selfDao() {
    return serviceRecordDao;
  }

  @Override
  @Transactional
  public void save(ServiceRecord entity) {
    try {
      super.save(entity);
    } catch (Exception e) {
      logger.error(e.getMessage());
      throw e;
    }
  }

  @Override
  @Transactional
  public ServiceRecord findById(Integer id) {
    try {
      return super.findById(id);
    } catch (Exception e) {
      logger.error(e.getMessage());
      return null;
    }
  }

  @Override
  @Transactional
  public List<ServiceRecord> findAll() {
    try {
      return super.findAll();
    } catch (Exception e) {
      logger.error(e.getMessage());
      return null;
    }
  }

  @Override
  @Transactional
  public void update(ServiceRecord entity) {
    try {
      super.update(entity);
    } catch (Exception e) {
      logger.error(e.getMessage());
      throw e;
    }
  }

  @Override
  @Transactional
  public void delete(ServiceRecord entity) {
    try {
      super.delete(entity);
    } catch (Exception e) {
      logger.error(e.getMessage());
      throw e;
    }
  }

  // TODO test
  @Transactional
  public ServiceRecord createServiceRecordForPrinter(Printer printer) {
    Objects.requireNonNull(printer);
    try {
      if (doesPrinterHaveActualServiceRecord(printer)) {
        throw new AlreadyExistsException("Actual Service record for printer " + printer.getId() + " already exists");
      }
      Optional<Invoice> invoice = invoiceService.findActualInvoiceForPrinter(printer);
      if (invoice.isPresent()) {
        ServiceRecord serviceRecord = new ServiceRecord(printer, invoice.get());
        serviceRecordDao.persist(serviceRecord);
        return serviceRecord;
      } else {
        throw new RuntimeException("Invoice for printer" + printer.getId() + "not found");
      }
    } catch (Exception e) {
      logger.error("createServiceRecordForPrinter: {}", e.getMessage());
      return null;
    }
  }

  @Transactional
  public void addRepairmentToServiceRecord(Repairment repairment, ServiceRecord serviceRecord) {
    Objects.requireNonNull(repairment);
    Objects.requireNonNull(serviceRecord);
    try {
      serviceRecord.addRepairment(repairment);
      serviceRecordDao.update(serviceRecord);
    } catch (Exception e) {
      logger.error(e.getMessage());
      throw e;
    }
  }

  @Transactional
  public void calculateTotalPrice(ServiceRecord serviceRecord) {
    try {
      serviceRecord.getRepairments().forEach(repairmentService::CalculatePrice);
      serviceRecord.setTotalPrice(serviceRecord.getRepairments().stream().mapToDouble(Repairment::getTotalPrice).sum());
      serviceRecordDao.update(serviceRecord);
    } catch (Exception e) {
      logger.error(e.getMessage());
      throw e;
    }
  }

  @Transactional
  public ServiceRecord findActualServiceRecordForPrinter(Printer printer) {
    Objects.requireNonNull(printer, "Printer cannot be null");
    try {
      Optional<Invoice> actualInvoice = invoiceService.findActualInvoiceForPrinter(printer);

      if (actualInvoice.isPresent()) {
        return serviceRecordDao.findByInvoice(actualInvoice.get())
            .stream()
            .filter(serviceRecord -> serviceRecord.getPrinter().equals(printer))
            .findFirst()
            .orElseThrow(() -> new RuntimeException(
                "No Service Record found for printer " + printer.getId() + " in the actual invoice"));
      } else {
        throw new RuntimeException("Invoice for printer " + printer.getId() + " not found");
      }
    } catch (Exception e) {
      logger.error("findActualServiceRecordForPrinter: {}", e.getMessage());
      return null;
    }
  }

  public boolean doesPrinterHaveActualServiceRecord(Printer printer) {
    Objects.requireNonNull(printer);
    Optional<Invoice> actualInvoice = invoiceService.findActualInvoiceForPrinter(printer);

    if (actualInvoice.isPresent()) {
      return serviceRecordDao.findByInvoice(actualInvoice.get())
          .stream()
          .anyMatch(serviceRecord -> serviceRecord.getPrinter()
              .equals(printer));
    } else {
      throw new RuntimeException("Invoice for printer " + printer.getId() + " not found");
    }
  }

}
