package cz.cvut.printer_rental.dao;

import cz.cvut.printer_rental.model.Customer;
import cz.cvut.printer_rental.model.Invoice;
import cz.cvut.printer_rental.model.Printer;
import cz.cvut.printer_rental.model.ServiceRecord;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Objects;

@Repository
public class ServiceRecordDao extends BaseDao<ServiceRecord> {
  public ServiceRecordDao() {
    super(ServiceRecord.class);
  }

  // Finds service records by Invoice
  public List<ServiceRecord> findByInvoice(Invoice invoice) {
    Objects.requireNonNull(invoice);
    return em.createNamedQuery("ServiceRecord.findByInvoice", ServiceRecord.class)
        .setParameter("invoice", invoice)
        .getResultList();
  }

}
