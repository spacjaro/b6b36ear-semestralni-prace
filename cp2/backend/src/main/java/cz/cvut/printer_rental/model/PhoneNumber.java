package cz.cvut.printer_rental.model;

import jakarta.persistence.Column;
import jakarta.persistence.Embeddable;
import jakarta.persistence.Id;
import lombok.Data;
import lombok.Getter;

@Getter
@Data
@Embeddable
public class PhoneNumber{
    @Column(name = "countryCode")
    private String countryCode;

    @Column(name = "phoneNumber")
    private String phoneNumber;

    public PhoneNumber() {}

    public PhoneNumber(String country, String number) {
        this.countryCode = country;
        this.phoneNumber = number;
    }

    public boolean equals(PhoneNumber other) {
        return (this.countryCode == other.countryCode && this.phoneNumber == other.phoneNumber);
    }

    /**
     * Returns a string representation of the phone number in the format "+countryCode phoneNumber".
     *
     * @return String representation of the phone number.
     */
    @Override
    public String toString() {
        StringBuilder b = new StringBuilder(16)
                .append('+')
                .append(countryCode)
                .append(' ');
        String s = String.valueOf(phoneNumber);
        b.append(s, 0, 3)
                .append(' ')
                .append(s, 3, 6)
                .append(' ')
                .append(s, 6, s.length());
        return b.toString();
    }
}

