package cz.cvut.printer_rental.dao;

import cz.cvut.printer_rental.model.Month;
import cz.cvut.printer_rental.model.QuarterYear;
import org.springframework.stereotype.Repository;

import java.util.Objects;

@Repository
public class QuarterYearDao extends BaseDao<QuarterYear> {
  public QuarterYearDao() {
    super(QuarterYear.class);
  }

  /**
   * Finds a QuarterYear based on the given year and month.
   *
   * @param year  the year to find the QuarterYear by.
   * @param month the month to find the QuarterYear by.
   * @return the QuarterYear matching the given year and month, or null if not
   *         found or in case of an error.
   */
  public QuarterYear findByYearAndLastMonth(int year, Month month) {
    Objects.requireNonNull(month);

    return em.createNamedQuery("QuarterYear.findByYearAndLastMonth", QuarterYear.class)
        .setParameter("year", year)
        .setParameter("month", month)
        .getSingleResult();

  }

  public QuarterYear findByYearAndFirstMonth(int year, Month month) {
    Objects.requireNonNull(month);

    return em.createNamedQuery("QuarterYear.findByYearAndFirstMonth", QuarterYear.class)
        .setParameter("year", year)
        .setParameter("month", month)
        .getSingleResult();

  }

}
