package cz.cvut.printer_rental.model;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@EqualsAndHashCode(callSuper = true)
@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor

@NamedQueries({

        @NamedQuery(name = "QuarterYear.findByYearAndLastMonth",
                query = "SELECT q FROM QuarterYear q WHERE q.currentYear = :year AND q.lastMonth = :month"),

        @NamedQuery(name = "QuarterYear.findByYearAndFirstMonth",
                query = "SELECT q FROM QuarterYear q WHERE q.currentYear = :year AND q.firstMonth = :month")
})
public class QuarterYear extends AbstractEntity {

    @Column(nullable = false)
    @Basic(optional = false)
    private int currentYear;

    @Column(nullable = false)
    @Basic(optional = false)
    private Month firstMonth;

    @Column(nullable = false)
    @Basic(optional = false)
    private Month lastMonth;

    public QuarterYear(int currentYear, int firstMonth) {
        this.currentYear = currentYear;
        this.firstMonth = Month.fromNumber(firstMonth);
        this.lastMonth = Month.fromNumber((firstMonth + 1)%12+1);
    }
    @Override
    public String toString() {
        return "\n"+"QuarterYear {"+"\n"+
                "   currentYear=" + currentYear + "\n" +
                "   firstMonth=" + firstMonth + "\n" +
                "   lastMonth=" + lastMonth +"\n"+
                "}";
    }

}
