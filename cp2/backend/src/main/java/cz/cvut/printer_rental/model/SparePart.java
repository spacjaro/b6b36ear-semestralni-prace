package cz.cvut.printer_rental.model;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.List;

@Entity
@EqualsAndHashCode(callSuper=false)
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name="SPARE_PARTS")

@NamedQueries({
        // Najde všechny náhradní díly pro konkrétní model, bez ohledu na množství
        @NamedQuery(name = "SparePart.findCompatibleForPrinterModel",
                query = "SELECT sp FROM SparePart sp " +
                        "JOIN sp.sparePartModel.CompatibleWithModels cm " +
                        "WHERE cm = :PrinterModel"),
        // Najde všechny tiskárny dle modelu
        @NamedQuery(name = "SparePart.findByModel",
                query = "SELECT sp FROM SparePart sp WHERE sp.sparePartModel = :model"),
})
public class SparePart extends AbstractEntity{

    @OneToOne
    @JoinColumn(name = "spare_part_model_id")
    private SparePartModel sparePartModel;

    @Column
    private int Amount = 0;

    public void setAmount(int amount) {
        if (amount >= 0) {
            Amount = amount;
        }else throw new IllegalArgumentException("Amount must be greater than or equal to 0");
    }

    public void increaseAmount(int amount){
        if (amount >= 0) {
            Amount+=amount;
        }else{
            throw new IllegalArgumentException("Amount must be greater than 0");
        }
    }
    public void decreaseAmount(int amount){
        if(Amount-amount >= 0){
            Amount-=amount;
        }else{
            throw new IllegalArgumentException("Only "+Amount+" of sparePart: "+ sparePartModel.getModelName()+" left");
        }

    }

    public void copyFrom(SparePart sparePartToCopy){
        this.Amount = sparePartToCopy.getAmount();
        this.sparePartModel = sparePartToCopy.getSparePartModel();
    }
}
