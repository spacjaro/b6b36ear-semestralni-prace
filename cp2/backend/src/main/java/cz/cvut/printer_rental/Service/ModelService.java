package cz.cvut.printer_rental.service;

import cz.cvut.printer_rental.dao.BaseDao;
import cz.cvut.printer_rental.dao.ModelDao;
import cz.cvut.printer_rental.exception.DuplicatePropertyException;
import cz.cvut.printer_rental.model.Brand;
import cz.cvut.printer_rental.model.Model;
import cz.cvut.printer_rental.model.PrinterModel;
import cz.cvut.printer_rental.model.SparePartModel;
import jakarta.persistence.Access;
import jakarta.persistence.EntityNotFoundException;
import jakarta.persistence.Table;
import jakarta.transaction.Transactional;
import lombok.Locked;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.method.P;
import org.springframework.stereotype.Service;

import java.lang.reflect.Field;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class ModelService extends AbstractCrudService<Model> {
  private final ModelDao modelDao;
  private static final Logger logger = LoggerFactory.getLogger(ModelService.class);

  @Autowired
  public ModelService(ModelDao modelDao) {
    this.modelDao = modelDao;
  }

  @Override
  protected BaseDao<Model> selfDao() {
    return modelDao;
  }

  @Override
  @Transactional
  public void save(Model entity) {
    Objects.requireNonNull(entity, "Model entity cannot be null");
    try {
      Objects.requireNonNull(modelDao);
      checkForDuplicates(entity);
      super.save(entity);
    } catch (DuplicatePropertyException e) {
      throw new DuplicatePropertyException(e.getMessage());
    } catch (Exception e) {
      logger.error(e.getMessage());
      throw e;
    }
  }

  @Override
  @Transactional
  public List<Model> findAll() {
    try {
      return super.findAll();
    } catch (Exception e) {
      logger.error(e.getMessage());
      return null;
    }
  }

  @Override
  @Transactional
  public void update(Model entity) {
    Objects.requireNonNull(entity, "Model entity cannot be null");
    try {
      checkForDuplicates(entity);
      super.update(entity);
    } catch (Exception e) {
      logger.error(e.getMessage());
      throw e;
    }
  }

  @Transactional
  public void deleteById(Integer id) {
    try {
      Model modelToDelete = super.findById(id);
      super.delete(modelToDelete);
    } catch (Exception e) {
      logger.error(e.getMessage());
      throw e;
    }
  }

  @Transactional
  public List<PrinterModel> getPrinterModels() {
    try {
      return findAll().stream()
          .filter(model -> model instanceof PrinterModel)
          .map(model -> (PrinterModel) model)
          .collect(Collectors.toList());
    } catch (Exception e) {
      logger.error(e.getMessage());
      return null;
    }
  }

  @Transactional
  public List<SparePartModel> getSparePartModels() {
    try {
      return findAll().stream()
          .filter(model -> model instanceof SparePartModel)
          .map(model -> (SparePartModel) model)
          .collect(Collectors.toList());
    } catch (Exception e) {
      logger.error(e.getMessage());
      return null;
    }
  }

  @Transactional
  public List<SparePartModel> getSparePartModelsForPrinterModelById(Integer modelId) {
    try {
      Model model = super.findById(modelId);

      if (!(model instanceof PrinterModel printerModel)) {
        logger.error("Model with ID {} is not a PrinterModel", modelId);
        return null; // or handle as needed, maybe return an empty list
      }
      return printerModel.getSparePartsModels();
    } catch (Exception e) {
      logger.error("Error occurred while retrieving spare part models for printer model with ID {}", modelId);
      return null; // return null or handle the error case as needed
    }
  }

  @Transactional
  public Model getModelByName(String modelName) {
    try {
      return modelDao.findByModelName(modelName);
    } catch (Exception e) {
      logger.error(e.getMessage());
      return null;
    }
  }

  @Transactional
  public SparePartModel getSparePartModelByID(Integer modelId) {
    try {
      Model model = super.findById(modelId);

      if (!(model instanceof SparePartModel sparePartModel)) {
        logger.error("Model with ID {} is not a SparePartModel", modelId);
        return null; // or handle as needed
      }
      return sparePartModel;
    } catch (Exception e) {
      logger.error("Error occurred while retrieving spare part model with ID {}", modelId);
      return null; // return null or a default value if necessary
    }
  }

  @Transactional
  public PrinterModel getPrinterModelByID(Integer modelId) {
    try {
      Model model = super.findById(modelId);

      if (!(model instanceof PrinterModel printerModel)) {
        logger.error("Model with ID {} is not a PrinterModel", modelId);
        return null; // or handle as needed
      }
      return printerModel;
    } catch (Exception e) {
      logger.error("Error occurred while retrieving printer model with ID {}", modelId);
      return null; // return null or a default value if necessary
    }
  }

  @Transactional
  public void AssignBrandToModel(Model model, Brand brand) {
    try {
      model.setBrand(brand);
      modelDao.update(model);
      logger.info("Model with ID {} assigned to {}", model.getId(), brand.getId());
    } catch (Exception e) {
      logger.error("Error occurred while assigning model to brand: Model ID {} and Brand ID {}",
          model.getId(), brand.getId(), e);
      throw e;
    }

  }

  private void checkForDuplicates(Model entity) {
    Objects.requireNonNull(entity); // Kontrola, že entita není null

    Model existingModel = null;
    try {
      // Snaží se najít existující model podle EAN
      existingModel = modelDao.findByEAN(entity.getEAN());
    } catch (Exception e) {
      // Pokud není nalezen žádný model, vypíše se zpráva
      logger.info("No duplicity found for EAN: {}", entity.getEAN());
    }

    // Pokud existuje model a má jiný ID, vyvolá se výjimka
    if (existingModel != null && !existingModel.getId().equals(entity.getId())) {
      throw new DuplicatePropertyException("Model with EAN " + entity.getEAN() + " already exists.");
    }
  }

}
