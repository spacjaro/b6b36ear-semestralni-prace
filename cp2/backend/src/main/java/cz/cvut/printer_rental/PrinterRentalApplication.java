package cz.cvut.printer_rental;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PrinterRentalApplication {

  public static void main(String[] args) {
    SpringApplication.run(PrinterRentalApplication.class, args);
  }
}
