package cz.cvut.printer_rental.exception;

public class NotAvailableException extends RuntimeException {
  public NotAvailableException(String message) {
    super(message);
  }
}
