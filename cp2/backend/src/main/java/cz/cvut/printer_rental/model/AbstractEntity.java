package cz.cvut.printer_rental.model;

import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.MappedSuperclass;
import lombok.Getter;
import lombok.Setter;
import java.io.Serializable;

/**
 * AbstractEntity is a base class for all entities in the application.
 * It provides a unique identifier for each entity.
 * Allows instances of this class to be serialized.
 * 
 * <hr />
 * Fields:
 * <ul>
 * <li>id: A unique identifier for the entity, automatically generated.</li>
 * </ul>
 */
@Setter
@Getter
@MappedSuperclass
public abstract class AbstractEntity implements Serializable {

  @Id
  @GeneratedValue
  private Integer id;
}