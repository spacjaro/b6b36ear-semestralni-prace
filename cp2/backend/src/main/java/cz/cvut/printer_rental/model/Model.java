package cz.cvut.printer_rental.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.*;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Entity
@EqualsAndHashCode(callSuper = true)
@Data
@Table(name = "MODEL")
@NamedQueries({
        @NamedQuery(name = "Model.findByEAN", query = "SELECT m FROM Model m WHERE m.EAN = :EAN"),
        @NamedQuery(name = "Model.findByModelName", query = "SELECT m FROM Model m WHERE m.ModelName = :MODEL_NAME")
})
public abstract class Model extends AbstractEntity{

    @ManyToOne
    @JoinColumn(name = "brand_id")
    @JsonBackReference
    private Brand brand;

    @Basic(optional = false)
    @Column
    private String ModelName;

    @Basic(optional = false)
    @Column
    private String Description;

    @Enumerated(EnumType.STRING)
    private Dimensions Dimensions;


    @Basic(optional = false)
    @Column(name = "EAN", unique = true)
    private String EAN;

    @Override
    public String toString() {
        return "Model{" + "\n" +
                "   Brand=" + brand.getBrandName() +"\n"+
                "   Model Name='" + ModelName + '\n' +
                "   Description='" + Description + '\n' +
                "   Dimensions=" + Dimensions +'\n'+
                "   EAN='" + EAN + '\n' +
                '}';
    }

    public void copyFrom(Model entityToCopy) {
        this.brand = entityToCopy.getBrand();
        this.ModelName = entityToCopy.getModelName();
        this.Description = entityToCopy.getDescription();
        this.Dimensions = entityToCopy.getDimensions();
    }
}
