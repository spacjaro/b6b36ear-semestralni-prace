package cz.cvut.printer_rental.rest;

import cz.cvut.printer_rental.service.UserService;
import cz.cvut.printer_rental.model.User;
import cz.cvut.printer_rental.security.SecurityUtils;
import cz.cvut.printer_rental.security.UserDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Objects;

@RestController
@RequestMapping("/api/v1/user")
public class UserController {

  private final UserService userService;

  @Autowired
  public UserController(UserService userService) {
    this.userService = userService;
  }

  @GetMapping("/details")
  public ResponseEntity<User> getCurrentUsername() {
    User user = Objects.requireNonNull(userService.getCurentUser());
    return ResponseEntity.ok(user);
  }
}
