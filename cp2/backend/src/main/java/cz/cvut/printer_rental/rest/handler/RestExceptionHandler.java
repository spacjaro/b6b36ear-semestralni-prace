package cz.cvut.printer_rental.rest.handler;

import cz.cvut.printer_rental.exception.DuplicatePropertyException;
import cz.cvut.printer_rental.exception.InvalidInputException;
import jakarta.persistence.PersistenceException;
import jakarta.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.crossstore.ChangeSetPersister;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

/**
 * Exception handlers for REST controllers.
 * <p>
 * The general pattern should be that unless an exception can be handled in a
 * more appropriate place it bubbles up to a
 * REST controller which originally received the request. There, it is caught by
 * this handler, logged and a reasonable
 * error message is returned to the user.
 */
@ControllerAdvice
public class RestExceptionHandler {

  private static final Logger logger = LoggerFactory.getLogger(RestExceptionHandler.class);

  private static ErrorInfo errorInfo(HttpServletRequest request, Throwable e) {
    return new ErrorInfo(e.getMessage(), request.getRequestURI());
  }

  @ExceptionHandler(PersistenceException.class)
  public ResponseEntity<ErrorInfo> persistenceException(HttpServletRequest request, PersistenceException e) {
    logger.error("PersistenceException: ", e);
    return new ResponseEntity<>(errorInfo(request, e.getCause()), HttpStatus.INTERNAL_SERVER_ERROR);
  }

  @ExceptionHandler(ChangeSetPersister.NotFoundException.class)
  public ResponseEntity<ErrorInfo> resourceNotFound(HttpServletRequest request,
      ChangeSetPersister.NotFoundException e) {
    return new ResponseEntity<>(errorInfo(request, e), HttpStatus.NOT_FOUND);
  }

  @ExceptionHandler(DuplicatePropertyException.class)
  public ResponseEntity<ErrorInfo> handleDuplicatePropertyException(HttpServletRequest request,
      DuplicatePropertyException e) {
    logger.error("DuplicatePropertyException: ", e);
    return new ResponseEntity<>(errorInfo(request, e), HttpStatus.CONFLICT); // 409 Conflict
  }

  @ExceptionHandler(RuntimeException.class)
  public ResponseEntity<ErrorInfo> handleRuntimeException(HttpServletRequest request, RuntimeException e) {
    logger.error("RuntimeException: ", e);
    return new ResponseEntity<>(errorInfo(request, e), HttpStatus.INTERNAL_SERVER_ERROR); // 500
  }

  @ExceptionHandler(Exception.class)
  public ResponseEntity<ErrorInfo> handleException(HttpServletRequest request, Exception e) {
    logger.error("Exception: ", e);
    return new ResponseEntity<>(errorInfo(request, e), HttpStatus.INTERNAL_SERVER_ERROR);
  }

  @ExceptionHandler(DataIntegrityViolationException.class)
  public ResponseEntity<ErrorInfo> handleDataIntegrityViolationException(HttpServletRequest request,
      DataIntegrityViolationException e) {
    logger.error("DataIntegrityViolationException: ", e);
    String message = "Operation cannot be completed because the resource is linked to other records.";
    return new ResponseEntity<>(errorInfo(request, e), HttpStatus.CONFLICT);
  }

  @ExceptionHandler(InvalidInputException.class)
  public ResponseEntity<ErrorInfo> handleInvalidInputException(HttpServletRequest request, InvalidInputException e) {
    logger.error("InvalidInputException: ", e);
    return new ResponseEntity<>(errorInfo(request, e), HttpStatus.CONFLICT);
  }

  @ExceptionHandler(IllegalArgumentException.class)
  public ResponseEntity<String> handleInsufficientSparePartsException(IllegalArgumentException ex) {
    logger.error("IllegalArgumentException", ex);
    return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ex.getMessage());
  }
}
