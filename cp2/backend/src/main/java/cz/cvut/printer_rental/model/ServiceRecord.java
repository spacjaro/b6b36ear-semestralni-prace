package cz.cvut.printer_rental.model;

import cz.cvut.printer_rental.exception.AlreadyPresentException;
import com.fasterxml.jackson.annotation.JsonBackReference;
import jakarta.persistence.*;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@EqualsAndHashCode(callSuper = true)
@Entity
@Data
@Table(name = "SERVICE_RECORDS")
@NamedQuery(name = "ServiceRecord.findByPrinter", query = "SELECT sr FROM ServiceRecord sr WHERE sr.printer = :printer")
@NamedQuery(name = "ServiceRecord.findByInvoice", query = "SELECT sr FROM ServiceRecord sr WHERE sr.invoice = :invoice")
public class ServiceRecord extends AbstractEntity {

  @Column(name = "totalPrice")
  private double totalPrice;

  @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
  // @OneToMany()
  @JsonBackReference
  private List<Repairment> repairments = new ArrayList<>();

  @ManyToOne
  @JoinColumn(name = "printer_id")
  private Printer printer;

  @ManyToOne
  @JoinColumn(name = "invoice_id")
  private Invoice invoice;

  public ServiceRecord() {
    this.repairments = new ArrayList<>();
    this.totalPrice = 0.0;
  }

  public ServiceRecord(Printer repairPrinter) {
    this.totalPrice = 0.0;
    this.printer = repairPrinter;
    this.repairments = new ArrayList<>();
  }

  public ServiceRecord(Printer repairPrinter, Invoice invoice) {
    this.totalPrice = 0.0;
    this.printer = repairPrinter;
    this.invoice = invoice;
    this.repairments = new ArrayList<>();
  }

  public void addRepairment(Repairment repairment) {
    Objects.requireNonNull(repairment);
    final Optional<Repairment> exist = repairments.stream().filter(r -> r.getId().equals(repairment.getId())).findAny();
    if (exist.isPresent()) {
      throw new AlreadyPresentException("Repairment already exists");
    } else {
      repairments.add(repairment);
    }
  }

  public void removeRepairment(Repairment repairment) {
    Objects.requireNonNull(repairment);
    final Optional<Repairment> exist = repairments.stream().filter(r -> r.getId().equals(repairment.getId())).findAny();
    if (exist.isPresent()) {
      repairments.remove(repairment);
    } else {
      throw new IllegalArgumentException("repairment not found");
    }
  }

  public void setRepairments(List<Repairment> repairments) {
    Objects.requireNonNull(repairments);

    if (repairments.isEmpty()) {
      throw new IllegalArgumentException("Repairments list must not be empty");
    } else {
      this.repairments = repairments;
    }
  }

}
