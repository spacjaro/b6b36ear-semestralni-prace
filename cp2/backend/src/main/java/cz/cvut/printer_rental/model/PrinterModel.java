package cz.cvut.printer_rental.model;


import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.*;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@EqualsAndHashCode(callSuper = true)
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name="modelType", discriminatorType = DiscriminatorType.STRING)
@Access(AccessType.FIELD)
public class PrinterModel extends Model{

    @ManyToMany(mappedBy = "CompatibleWithModels")
    @JsonIgnore
    private List<SparePartModel> sparePartsModels;

    @Column
    private boolean IsColor;

    @Column
    private boolean IsDuplex;

    @Column
    private boolean IsScanner;

    @Column
    private boolean IsWireless;

    @ElementCollection
    @Column(name = "paper_sizes")
    private List<String> paperSizes = new ArrayList<>();

    @Column
    private double printSpeed;

    @Column
    private int resolution;

    public List<PaperSize> getPaperSize() {
        return paperSizes.stream()
                .map(PaperSize::valueOf)
                .collect(Collectors.toList());
    }

    public void setPaperSize(List<PaperSize> sizes) {
        this.paperSizes = sizes.stream()
                .map(Enum::name)
                .collect(Collectors.toList());
    }

    @Override
    public String toString() {
        return "PrinterModel{" + "\n"+
                "  "+super.getBrand().getBrandName() + "\n" +
                "  "+super.getModelName() + "\n" +
                "  "+super.getEAN() + "\n" +
                "    IsColor=" + IsColor + "\n" +
                "    IsDuplex=" + IsDuplex + "\n" +
                "    IsScanner=" + IsScanner + "\n" +
                "    IsWireless=" + IsWireless + "\n" +
                "    PrintSpeed=" + printSpeed + "\n" +
                "    Resolution=" + resolution + "\n" +
                "    PaperSizes=" + paperSizes + "\n" +
                "    CompatibleWith "+ (sparePartsModels != null?sparePartsModels.size():"\u001B[31m<null>\u001B[0m") +"\n" +
                '}';
    }

    public void addSparePartModel(SparePartModel sparePartModel) {
        if (sparePartsModels == null) {
            sparePartsModels = new ArrayList<>();
        }
        if (!sparePartsModels.contains(sparePartModel)) {
            sparePartsModels.add(sparePartModel);
        }
    }

}
