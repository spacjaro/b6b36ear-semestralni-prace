package cz.cvut.printer_rental.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import cz.cvut.printer_rental.exception.AlreadyExistsException;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.net.URL;
import java.util.*;
import java.util.stream.Collectors;

@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "BRAND")
@NoArgsConstructor
@AllArgsConstructor
@Data
@NamedQueries({
    @NamedQuery(name = "Brand.findByName", query = "SELECT b FROM Brand b WHERE b.brandName = :brandName")
})
public class Brand extends AbstractEntity {

  @Column(name = "brandName", unique = true, nullable = false)
  @Basic(optional = false)
  private String brandName;

  @Column
  private URL companyWebsite;

  public Brand(String brandName) {
    this.brandName = brandName;
  }

  @Override
  public String toString() {
    return "Brand{\n" +
        "  Brand Name='" + brandName + "',\n";
  }
}
