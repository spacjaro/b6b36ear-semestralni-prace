package cz.cvut.printer_rental.model;

import cz.cvut.printer_rental.exception.InvalidInputException;
import com.fasterxml.jackson.annotation.JsonBackReference;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.Objects;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "REPAIR_ITEM")
public class RepairItem extends AbstractEntity {

  @Column
  @Basic(optional = false)
  private Integer amount;

  @Column
  @Basic(optional = false)
  private double price_per_unit;

  @ManyToOne
  @JoinColumn(name = "spare_part_id", nullable = false)
  @JsonBackReference
  private SparePart sparePart;

  public RepairItem(SparePart part) {
    sparePart = part;
    amount = 1;
    price_per_unit = 1;
  }

  public void increaseAmount() {
    amount++;
  }

  public void decreaseAmount() {
    if (amount > 0) {
      amount--;
    } else {
      amount = 0;
      throw new InvalidInputException("Amount is 0");
    }

  }

  public void setAmount(int amount) {
    assert amount > 0;
    this.amount = amount;
  }

  public void setPrice(double price) {
    if (price < 0) {
      throw new IllegalArgumentException("Price must be greater than zero");
    }
    this.price_per_unit = price;
  }
}
