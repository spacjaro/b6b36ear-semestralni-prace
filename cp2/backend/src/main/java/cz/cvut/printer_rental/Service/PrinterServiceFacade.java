package cz.cvut.printer_rental.service;

import cz.cvut.printer_rental.model.Printer;
import cz.cvut.printer_rental.model.RepairItem;
import cz.cvut.printer_rental.model.Repairment;
import cz.cvut.printer_rental.model.ServiceRecord;
import jakarta.persistence.Access;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PrinterServiceFacade {
  private static final Logger logger = LoggerFactory.getLogger(PrinterServiceFacade.class);
  private final PrinterService printerService;
  private final CustomerService customerService;
  private final InvoiceService invoiceService;
  private final ServiceRecordService serviceRecordService;
  private final RepairmentService repairmentService;
  private final RepairItemService repairItemService;

  @Autowired
  public PrinterServiceFacade(PrinterService printerService, CustomerService customerService,
      InvoiceService invoiceService, ServiceRecordService serviceRecordService, RepairmentService repairmentService,
      RepairItemService repairItemService) {
    this.printerService = printerService;
    this.customerService = customerService;
    this.invoiceService = invoiceService;
    this.serviceRecordService = serviceRecordService;
    this.repairmentService = repairmentService;
    this.repairItemService = repairItemService;
  }

  // {
  // "printerID": "printerId",
  // "description": "description",
  // "workPrice": "workPrice",
  // "items": [
  // {
  // "sparePartId": "id1",
  // "amount": "amount1",
  // "price": "price1"
  // },
  // {
  // "sparePartId": "id2",
  // "amount": "amount2",
  // "price": "price2"
  // }
  // ]
  // }

  public void createRepairmentForPrinter(Integer printerId, String description, int workPrice,
      List<RepairItem> repairItems) {
    try {
      Printer printer = printerService.findById(printerId);
      ServiceRecord serviceRecord = null;
      // IF Service Record existing for this Printer in current Quarter Year related
      // to Customer actual Invoice
      if (serviceRecordService.doesPrinterHaveActualServiceRecord(printer)) {
        // FIND it
        serviceRecord = serviceRecordService.findActualServiceRecordForPrinter(printer);
      } else {
        // ELSE create one
        serviceRecord = serviceRecordService.createServiceRecordForPrinter(printer);
      }
      // CREATE and add Repairment with Repair Items and Calculate() total price
      Repairment repairment = repairmentService.CreateRepairment(repairItems, description, workPrice);
      serviceRecordService.addRepairmentToServiceRecord(repairment, serviceRecord);
      serviceRecordService.calculateTotalPrice(serviceRecord);
    } catch (Exception e) {
      logger.error(e.getMessage());
      throw e;
    }
  }

}
