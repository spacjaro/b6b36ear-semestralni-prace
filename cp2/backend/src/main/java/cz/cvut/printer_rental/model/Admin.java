package cz.cvut.printer_rental.model;

import jakarta.persistence.*;

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name="userType", discriminatorType = DiscriminatorType.INTEGER)
@Access(AccessType.FIELD)
public class Admin extends User{
    private static final Role role =Role.ADMIN;
    @Override
    public Role getRole() {
        return role;
    }
    public Admin() {
        super();
    }
    /**
     * Constructor for the Admin class that initializes it based on another User object.
     * @param user Another User object whose properties are used to initialize this Admin object.
     */
    public Admin(User user) {
        super(user);
    }
}
