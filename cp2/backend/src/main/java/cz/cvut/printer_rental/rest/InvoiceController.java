package cz.cvut.printer_rental.rest;

import cz.cvut.printer_rental.model.Invoice;
import cz.cvut.printer_rental.service.InvoiceService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/invoices")
public class InvoiceController {
  private static final Logger LOG = LoggerFactory.getLogger(InvoiceController.class);

  private final InvoiceService invoiceService;

  @Autowired
  public InvoiceController(InvoiceService invoiceService) {
    this.invoiceService = invoiceService;
  }

  // Get all invoices
  @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
  public List<Invoice> getAllInvoices(
      @RequestParam(required = false) Integer customerId,
      @RequestParam(required = false) Integer quarterYearId,
      @RequestParam(required = false) Boolean paid) {

    List<Invoice> invoices;

    if (customerId != null) {
      invoices = invoiceService.findInvoicesByCustomerId(customerId);
    } else if (quarterYearId != null) {
      invoices = invoiceService.findInvoicesByQuarterYearId(quarterYearId);
    } else if (paid != null) {
      invoices = paid ? invoiceService.findPaidInvoices() : invoiceService.findUnpaidInvoices();
    } else {
      invoices = invoiceService.findAll();
    }

    return invoices;
  }

  // Get invoice by ID
  @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<Invoice> getInvoice(@PathVariable Integer id) {
    Invoice invoice = invoiceService.findById(id);
    if (invoice == null) {
      LOG.warn("Invoice not found with id {}", id);
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
    return ResponseEntity.ok(invoice);
  }

  // Create a new invoice
  @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<Invoice> createInvoice(@RequestBody Invoice invoice) {
    invoiceService.save(invoice);
    LOG.debug("Invoice created: {}", invoice);
    return new ResponseEntity<>(invoice, HttpStatus.CREATED);
  }

  // Update an existing invoice
  @PutMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<Invoice> updateInvoice(@PathVariable Integer id, @RequestBody Invoice invoice) {
    invoice.setId(id);
    invoiceService.update(invoice);
    LOG.debug("Invoice updated: {}", invoice);
    return new ResponseEntity<>(invoice, HttpStatus.OK);
  }

  // Delete an invoice
  @DeleteMapping(path = "/{id}")
  public ResponseEntity<Void> deleteInvoice(@PathVariable Integer id) {
    invoiceService.deleteById(id);
    LOG.debug("Invoice deleted with id: {}", id);
    return new ResponseEntity<>(HttpStatus.NO_CONTENT);
  }
}
