package cz.cvut.printer_rental.config;

import cz.cvut.printer_rental.dao.UserDao;
import cz.cvut.printer_rental.service.UserService;
import cz.cvut.printer_rental.model.PhoneNumber;
import cz.cvut.printer_rental.model.SuperAdmin;
import cz.cvut.printer_rental.model.User;
import jakarta.transaction.Transactional;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.Collections;

@Slf4j
@Component
public class DataInitializer implements CommandLineRunner {

  private final PasswordEncoder passwordEncoder;
  private final UserService userService;

  @Autowired
  public DataInitializer(UserDao userRepository, PasswordEncoder passwordEncoder, UserService userService) {
    this.passwordEncoder = passwordEncoder;
    this.userService = userService;
  }

  @Override
  public void run(String... args) throws Exception {

    try {
      if (userService.findByUsername("superadmin") == null) {
        log.info("Creating SuperAdmin");
        User superAdmin = new SuperAdmin();
        superAdmin.setUsername("superadmin");
        superAdmin.setPassword(passwordEncoder.encode("123"));
        superAdmin.setFirstName("Jonas");
        superAdmin.setLastName("Sanislo");
        superAdmin.setEmail("superadmin@gmail.com");
        superAdmin.setPhoneNumber(new PhoneNumber("420", "778009067"));
        log.info("Saving SuperAdmin");
        userService.save(superAdmin);
      } else {
        log.info("SuperAdmin already exists");
      }
    } catch (Exception e) {
      log.error("No superadmin found");
    }
  }
}
