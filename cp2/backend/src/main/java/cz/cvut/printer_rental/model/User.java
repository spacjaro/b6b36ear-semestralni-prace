package cz.cvut.printer_rental.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.security.crypto.password.PasswordEncoder;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@Table(name = "RENTAL_USER")

@NamedQueries({
        @NamedQuery(name = "User.findByEmail", query = "SELECT u FROM User u WHERE u.email = :email"),
        @NamedQuery(name = "User.findByUsername", query = "SELECT u FROM User u WHERE u.username = :username"),
        @NamedQuery(name = "User.findByTel", query = "SELECT u FROM User u WHERE u.phoneNumber = :phoneNumber")
})
public abstract class User extends AbstractEntity {

    @Basic(optional = false)
    @Column(nullable = false, unique = true)
    protected String username;

    @Basic(optional = false)
    @Column(name="firstname", nullable = false)
    protected String firstName;

    @Basic(optional = false)
    @Column(name = "lastname", nullable = false)
    protected String lastName;

    @Basic(optional = false)
    @Column(name="email", nullable = false, unique = true)
    protected String email;

    @Basic(optional = false)
    @Column(name = "password", nullable = false)
    @JsonIgnore
    protected String password;

    @Embedded
    @Basic(optional = false)
    @Column(unique = true, nullable = false)
    protected PhoneNumber phoneNumber;

    public User() {
    }

    public User(User u) {
        this.firstName = u.firstName;
        this.lastName = u.lastName;
        this.phoneNumber = u.phoneNumber;
        this.email = u.email;
        this.password = u.password;
        this.setId(u.getId());
    }

    public abstract Role getRole();

    public void encodePassword(PasswordEncoder encoder) {
        this.password = encoder.encode(password);
    }
}
