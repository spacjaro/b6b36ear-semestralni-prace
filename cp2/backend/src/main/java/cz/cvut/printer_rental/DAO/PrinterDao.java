package cz.cvut.printer_rental.dao;

import cz.cvut.printer_rental.model.*;
import org.springframework.stereotype.Repository;
import jakarta.persistence.NoResultException;

import java.util.List;
import java.util.Objects;

@Repository
public class PrinterDao extends BaseDao<Printer> {
  public PrinterDao() {
    super(Printer.class);
  }

  // Finds all printers belonging to a specific customer
  public List<Printer> findByCustomer(Customer customer) {
    Objects.requireNonNull(customer);
    return em.createNamedQuery("Printer.findByCustomer", Printer.class)
        .setParameter("customer", customer)
        .getResultList();
  }

  // Finds all printers by model
  public List<Printer> findByModel(PrinterModel model) {
    Objects.requireNonNull(model);
    return em.createNamedQuery("Printer.findByModel", Printer.class)
        .setParameter("model", model)
        .getResultList();

  }

  // Finds all printers by brand
  public List<Printer> findByBrand(Brand brand) {
    Objects.requireNonNull(brand);
    return em.createNamedQuery("Printer.findByBrand", Printer.class)
        .setParameter("brand", brand)
        .getResultList();

  }

  // Finds all free printers
  public List<Printer> findFree() {
    return em.createNamedQuery("Printer.findFree", Printer.class)
        .getResultList();
  }

  // Finds a printer by serial number
  public Printer findBySerialNumber(String serialNumber) {
    Objects.requireNonNull(serialNumber);
    try {
      return em.createNamedQuery("Printer.findBySerialNumber", Printer.class)
          .setParameter("SerialNumber", serialNumber)
          .getSingleResult();
    } catch (NoResultException e) {
      return null; // Return null if no result is found
    }
  }
}
