package cz.cvut.printer_rental.service;

import cz.cvut.printer_rental.dao.BaseDao;
import cz.cvut.printer_rental.dao.CustomerDao;
import cz.cvut.printer_rental.dao.InvoiceDao;
import cz.cvut.printer_rental.exception.DuplicatePropertyException;
import cz.cvut.printer_rental.exception.NotAvailableException;
import cz.cvut.printer_rental.model.Customer;
import cz.cvut.printer_rental.model.Invoice;
import cz.cvut.printer_rental.model.Printer;
import cz.cvut.printer_rental.model.QuarterYear;
import cz.cvut.printer_rental.model.PhoneNumber;
import jakarta.transaction.Transactional;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import org.slf4j.Logger;

@Service
public class CustomerService extends AbstractCrudService<Customer> {

  private static final Logger logger = LoggerFactory.getLogger(CustomerService.class);

  // Dao
  private final CustomerDao customerDao;

  // Service
  private final PrinterService printerService;
  private final QuarterYearService quarterYearService;
  private final InvoiceService invoiceService;

  @Autowired
  public CustomerService(
      CustomerDao dao,
      PrinterService printerService,
      InvoiceService invoiceService,
      QuarterYearService quarterYearService) {
    this.customerDao = dao;
    this.printerService = printerService;
    this.invoiceService = invoiceService;
    this.quarterYearService = quarterYearService;
  }

  @Override
  protected BaseDao<Customer> selfDao() {
    return customerDao;
  }

  @Override
  @Transactional
  public void save(Customer entity) {
    Objects.requireNonNull(entity, "Customer cannot be null");
    try {
      checkForDuplicates(entity);
      super.save(entity);
      invoiceService.initialInvoiceAssigment(entity);
    } catch (DuplicatePropertyException e) {
      logger.warn(e.getMessage());
      throw e;
    } catch (Exception e) {
      logger.error(e.getMessage());
      throw new RuntimeException("Failed to save customer", e);
    }
  }

  @Override
  @Transactional
  public void update(Customer entity) {
    Objects.requireNonNull(entity, "Customer cannot be null");
    try {
      checkForDuplicates(entity);
      if (customerDao.exists(entity.getId())) {
        Customer existing = super.findById(entity.getId());
        Objects.requireNonNull(existing);
        existing.copyFrom(entity);
        customerDao.update(existing);
        return;
      }
      super.update(entity);
    } catch (DuplicatePropertyException e) {
      logger.warn(e.getMessage());
      throw e;
    } catch (Exception e) {
      logger.error(e.getMessage());
      throw e;
    }
  }

  @Override
  @Transactional
  public void delete(Customer entity) {
    try {
      if (invoiceService.findInvoicesByCustomerId(entity.getId()).size() == 1) {
        invoiceService.delete(invoiceService.findInvoicesByCustomerId(entity.getId()).get(0));
      }
      super.delete(entity);
    } catch (Exception e) {
      logger.error(e.getMessage());
      throw e;
    }
  }

  @Transactional
  public void deleteById(Integer id) {
    try {
      Customer customer = super.findById(id);
      delete(customer);
    } catch (Exception e) {
      logger.error(e.getMessage());
      throw e;
    }
  }

  /**
   * Najít zákazníka podle názvu společnosti.
   *
   * @param companyName Název společnosti
   * @return Customer
   */
  @Transactional
  public Customer getCustomerByCompanyName(String companyName) {
    try {
      return customerDao.findByCompanyName(companyName);
    } catch (Exception e) {
      logger.error("Error occurred while finding customer by company name: {}", companyName);
      return null; // return null if customer is not found or an error occurs
    }
  }

  /**
   * Najít zákazníky podle křestního jména.
   *
   * @param firstName Křestní jméno
   * @return Seznam zákazníků s daným křestním jménem
   */
  @Transactional
  public List<Customer> getCustomersByFirstName(String firstName) {
    try {
      return customerDao.findByFirstName(firstName);
    } catch (Exception e) {
      logger.error("Error occurred while finding customers by first name: {}", firstName);
      return null; // return null if no customers are found or an error occurs
    }
  }

  /**
   * Najít zákazníky podle příjmení.
   *
   * @param lastName Příjmení
   * @return Seznam zákazníků s daným příjmením
   */
  @Transactional
  public List<Customer> getCustomersByLastName(String lastName) {
    try {
      return customerDao.findByLastName(lastName);
    } catch (Exception e) {
      logger.error("Error occurred while finding customers by last name: {}", lastName);
      return null; // return null if no customers are found or an error occurs
    }
  }

  @Transactional
  public Customer getCustomerByICO(String ICO) {
    Objects.requireNonNull(ICO, "ICO cannot be null");
    try {
      return customerDao.findByICO(ICO);
    } catch (Exception e) {
      logger.error("Error occurred while finding customer by ICO: {}", ICO, e);
      return null;
    }
  }

  @Transactional
  public Customer getCustomerByDIC(String DIC) {
    Objects.requireNonNull(DIC, "DIC cannot be null");
    try {
      return customerDao.findByDIC(DIC);
    } catch (Exception e) {
      logger.error("Error occurred while finding customer by DIC: {}", DIC, e);
      return null;
    }
  }

  @Transactional
  public Customer getCustomerByEmail(String email) {
    Objects.requireNonNull(email, "Email cannot be null");
    try {
      return customerDao.findByEmail(email);
    } catch (Exception e) {
      logger.error("Error occurred while finding customer by email: {}", email, e);
      return null;
    }
  }

  @Transactional
  public Customer getCustomerByPhoneNumber(PhoneNumber phoneNumber) {
    Objects.requireNonNull(phoneNumber, "Phone number cannot be null");
    try {
      return customerDao.findByPhoneNumber(phoneNumber);
    } catch (Exception e) {
      logger.error("Error occurred while finding customer by phone number: {}", phoneNumber, e);
      return null;
    }
  }

  /**
   * Najít zákazníky podle čtvrtletí a roku.
   *
   * @param quarterYear Čtvrtletí a rok
   * @return Seznam zákazníků odpovídajících čtvrtletí a roku
   */
  @Transactional
  public List<Customer> getCustomersByQuarterYear(QuarterYear quarterYear) {
    try {
      return customerDao.findByQuarterYear(quarterYear);
    } catch (Exception e) {
      logger.error("Error occurred while finding customers by quarter and year: {}", quarterYear);
      return null; // return null if no customers are found or an error occurs
    }
  }

  /*
   * Zkontroluje existenci entit a integritu. Nepovoli pridat @Printer pokud jiz
   * je ve vlastnicvi jineho @Customer
   */
  @Transactional
  public void assignPrinterToCustomer(Integer customerID, Integer printerID) {
    Objects.requireNonNull(customerID, "Customer ID cannot be null");
    Objects.requireNonNull(printerID, "Printer ID cannot be null");

    try {
      Customer customer = super.findById(customerID);
      Printer printer = printerService.findById(printerID);

      /*
       * IF is printer already assigned to some Customer and the Customer is NOT
       * current customer
       */
      if (printer.getCustomer() != null && !(printer.getCustomer().getId().equals(customer.getId()))) {

        logger.error("Printer ID: {} is not available. Already belongs to Customer ID: {}", printerID,
            printer.getCustomer().getId());
        throw new NotAvailableException(
            "Can not assign Printer ID: " + printerID + " is not available. Already belongs to Customer ID: "
                + printer.getCustomer().getId());
      } else {
        customer.addPrinterToCustomer(printer);
        printerService.AssignCustomerToPrinter(customer, printer);
        customerDao.update(customer);
        logger.debug("Successfully assigned printer {} to customer {}", printerID, customerID);

      }
    } catch (Exception e) {
      logger.error("Failed to assign printer {} to customer {}: {}", printerID, customerID, e.getMessage(), e);
      throw e;
    }
  }

  /*
   * Zkontroluje existenci entit a integritu. Nepovoli odebrat @Printer pokud
   * skutecne neni ve vlastnictvi @Customer
   */
  @Transactional
  public void removePrinterFromCustomer(Integer customerID, Integer printerID) {
    Objects.requireNonNull(customerID, "Customer ID cannot be null");
    Objects.requireNonNull(printerID, "Printer ID cannot be null");

    try {
      Customer customer = super.findById(customerID);
      Printer printer = printerService.findById(printerID);

      if (!printer.getCustomer().getId().equals(customer.getId())) {
        logger.error("Printer ID: {} is not available. Already belongs to Customer ID: {}}",
            printerID, printer.getCustomer().getId());
        throw new NotAvailableException(
            "Can not remove Printer ID: " + printerID + " is not available. Already belongs to Customer ID: "
                + printer.getCustomer().getId());

      } else {
        customer.removePrinterFromCustomer(printer);
        customerDao.update(customer);
        printer.setCustomer(null);
        printerService.unassignCustomerFromPrinter(printer);
        logger.debug("Successfully removed printer {} from customer {}", printerID, customerID);
      }
    } catch (Exception e) {
      logger.error("Failed to remove printer {} from customer {}: {}", printerID, customerID, e.getMessage(), e);
      throw e;
    }
  }

  @Transactional
  private void checkForDuplicates(Customer entity) {
    Objects.requireNonNull(entity); // Kontrola, že entita není null

    Customer existingCustomer = null;

    try {
      // Kontrola duplicity podle názvu firmy
      existingCustomer = customerDao.findByCompanyName(entity.getCompanyName());
    } catch (Exception e) {
      logger.info("No duplicity found for company name: {}", entity.getCompanyName());
    }
    if (existingCustomer != null && !(existingCustomer.getId().equals(entity.getId()))) {
      throw new DuplicatePropertyException(
          "Customer with company name '" + entity.getCompanyName() + "' already exists.");
    }

    try {
      // Kontrola duplicity podle ICO
      existingCustomer = customerDao.findByICO(entity.getIco());
    } catch (Exception e) {
      logger.info("No duplicity found for ICO: {}", entity.getIco());
    }
    if (existingCustomer != null && !(existingCustomer.getId().equals(entity.getId()))) {
      throw new DuplicatePropertyException(
          "Customer with ICO '" + entity.getIco() + "' already exists.");
    }

    try {
      // Kontrola duplicity podle DIC
      existingCustomer = customerDao.findByDIC(entity.getDic());
    } catch (Exception e) {
      logger.info("No duplicity found for DIC: {}", entity.getDic());
    }
    if (existingCustomer != null && !(existingCustomer.getId().equals(entity.getId()))) {
      throw new DuplicatePropertyException(
          "Customer with DIC '" + entity.getDic() + "' already exists.");
    }

    try {
      // Kontrola duplicity podle emailu
      existingCustomer = customerDao.findByEmail(entity.getEmail());
    } catch (Exception e) {
      logger.info("No duplicity found for email: {}", entity.getEmail());
    }
    if (existingCustomer != null && !(existingCustomer.getId().equals(entity.getId()))) {
      throw new DuplicatePropertyException(
          "Customer with email '" + entity.getEmail() + "' already exists.");
    }

    try {
      // Kontrola duplicity podle telefonního čísla
      existingCustomer = customerDao.findByPhoneNumber(entity.getPhoneNumber());
    } catch (Exception e) {
      logger.info("No duplicity found for phone number: {}", entity.getPhoneNumber());
    }
    if (existingCustomer != null && !(existingCustomer.getId().equals(entity.getId()))) {
      throw new DuplicatePropertyException(
          "Customer with phone number '" + entity.getPhoneNumber() + "' already exists.");
    }
  }

}
