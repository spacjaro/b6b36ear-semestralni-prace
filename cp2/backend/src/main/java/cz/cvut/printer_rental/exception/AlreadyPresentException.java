package cz.cvut.printer_rental.exception;

public class AlreadyPresentException extends RuntimeException {
  public AlreadyPresentException(String message) {
    super(message);
  }
}
