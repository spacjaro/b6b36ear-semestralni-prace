package cz.cvut.printer_rental.model;

public enum Month {
    LEDEN(1, "Leden"),
    UNOR(2, "Únor"),
    BREZEN(3, "Březen"),
    DUBEN(4, "Duben"),
    KVETEN(5, "Květen"),
    CERVEN(6, "Červen"),

    CERVENEC(7, "Červenec"),
    SRPEN(8, "Srpen"),
    ZARI(9, "Září"),
    RIJEN(10, "Říjen"),
    LISTOPAD(11, "Listopad"),
    PROSINEC(12, "Prosinec");

    private final int monthNumber;
    private final String monthName;

    Month(int monthNumber, String monthName) {
        this.monthNumber = monthNumber;
        this.monthName = monthName;
    }

    public int getMonthNumber() {
        return monthNumber;
    }

    public String getMonthName() {
        return monthName;
    }
    public static Month fromNumber(int monthNumber) {
        for (Month month : Month.values()) {
            if (month.monthNumber == monthNumber) {
                return month;
            }
        }
        throw new IllegalArgumentException("Invalid month number: " + monthNumber);
    }
    public static Month nextMonth(Month month) {
        return Month.values()[(month.ordinal() + 1) % Month.values().length];
    }
    @Override
    public String toString() {
        return monthName + " (" + monthNumber + ")";
    }

}
