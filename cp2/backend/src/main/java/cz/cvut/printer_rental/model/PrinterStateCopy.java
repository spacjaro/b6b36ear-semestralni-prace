package cz.cvut.printer_rental.model;

import jakarta.persistence.*;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Entity
@Data
public class PrinterStateCopy extends AbstractEntity {

    // Konstruktor, který přijímá objekt Printer a naplňuje hodnoty
    public PrinterStateCopy(Printer printer) {
        this.printer = printer;
        this.numOfBWCopy = printer.getNumOfBWCopy();
        this.currentNumOfBWCopy = printer.getCURRENTNumOfBWCopy();

        this.numOfColorCopy = printer.getNumOfColorCopy();
        this.currentNumOfColorCopy = printer.getCURRENTNumOfColorCopy();

        this.rentPrice = printer.getRent_price();

        this.bwPricePerCopy = printer.getBw_price_per_copy();
        this.colorPricePerCopy = printer.getColor_price_per_copy();
        calculateTotalPrice();
    }
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "printer_id", nullable = false)
    private Printer printer; // Tiskárna, která má tento stav

    @Column(nullable = false)
    private int numOfBWCopy; // Počet černobílých kopií při odečtu

    @Column(nullable = false)
    private int currentNumOfBWCopy; // Aktuální počet černobílých kopií při odečtu

    @Column(nullable = false)
    private int numOfColorCopy; // Počet barevných kopií při odečtu

    @Column(nullable = false)
    private int currentNumOfColorCopy; // Aktuální počet barevných kopií při odečtu

    @Column(nullable = false)
    private double rentPrice; // Cenu za pronájem při odečtu

    @Column(nullable = false)
    private double bwPricePerCopy; // Cena za černobílou kopii při odečtu

    @Column(nullable = false)
    private double colorPricePerCopy; // Cena za barevnou kopii při odečtu

    @Column(nullable = false)
    private double totalPrice; // Cena za barevnou kopii při odečtu

    public PrinterStateCopy() {
    }

    private void calculateTotalPrice() {
        this.totalPrice = rentPrice
                + (currentNumOfBWCopy-numOfBWCopy)*bwPricePerCopy
                + (currentNumOfColorCopy-numOfColorCopy)*colorPricePerCopy;
    }
}
