package cz.cvut.printer_rental.service;

import cz.cvut.printer_rental.dao.CustomerDao;
import cz.cvut.printer_rental.dao.InvoiceDao;
import cz.cvut.printer_rental.dao.PrinterStateCopyDao;
import cz.cvut.printer_rental.dao.ServiceRecordDao;
import cz.cvut.printer_rental.model.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class FacturationService {

  private static final Logger logger = LoggerFactory.getLogger(FacturationService.class);

  private final InvoiceDao invoiceDao;
  private final PrinterStateCopyDao printerStateCopyDao;
  private final ServiceRecordDao serviceRecordDao;

  private final PrinterService printerService;
  private final ServiceRecordService serviceRecordService;
  private final QuarterYearService quarterYearService;

  @Autowired
  public FacturationService(
      InvoiceDao invoiceDao,
      PrinterStateCopyDao printerStateCopyDao,
      ServiceRecordDao serviceRecordDao,
      PrinterService printerService,
      ServiceRecordService serviceRecordService,
      QuarterYearService quarterYearService) {
    this.invoiceDao = invoiceDao;
    this.printerStateCopyDao = printerStateCopyDao;
    this.serviceRecordDao = serviceRecordDao;
    this.printerService = printerService;
    this.serviceRecordService = serviceRecordService;
    this.quarterYearService = quarterYearService;
  }

  @Transactional
  public void calculateInvoicePrinters(Integer invoiceId) throws IllegalAccessException {
    try {
      // Find Invoice
      Invoice invoice = invoiceDao.find(invoiceId);
      Objects.requireNonNull(invoice);
      if (!invoice.isActual()) {
        throw new IllegalAccessException("Invoice is not actual");
      }

      // Delete old PrinterStateCopies
      List<PrinterStateCopy> oldPrinterStateCopies = invoice.getPrinterStateCopies();
      if (oldPrinterStateCopies != null && !oldPrinterStateCopies.isEmpty()) {
        oldPrinterStateCopies.forEach(printerStateCopyDao::remove);
      }

      // Get Customer and Printers
      Customer customer = invoice.getCustomer();
      Objects.requireNonNull(customer);
      List<Printer> customersPrinters = customer.getPrinters();
      if (customersPrinters == null) {
        invoice.setTotalPrice(0);
        invoice.setDateOfCreation(LocalDateTime.now());
        invoiceDao.update(invoice);
        return;
      }

      // Make printerStateCopies
      List<PrinterStateCopy> printerStateCopies = customersPrinters.stream()
          .map(PrinterStateCopy::new)
          .toList();

      // Save PrinterStetsCopies and set
      printerStateCopies.forEach(printerStateCopyDao::persist);
      // invoice.setPrinterStateCopies(printerStateCopies);
      invoice.setPrinterStateCopies(new ArrayList<>(printerStateCopies));

      double sum = printerStateCopies.stream()
          .mapToDouble(PrinterStateCopy::getTotalPrice) // Získání totalPrice pro každou tiskárnu
          .sum(); // Součet všech cen z printerStateCopies

      // //TODO test
      try {
        sum += serviceRecordDao.findByInvoice(invoice).stream()
            .peek(serviceRecordService::calculateTotalPrice)
            .mapToDouble(ServiceRecord::getTotalPrice)
            .sum();
      } catch (Exception e) {
        logger.warn("No Service Record in invoice {} for customer {}", invoice.getId(), invoice.getCustomer().getId());
      }

      invoice.setTotalPrice(sum);
      invoice.setDateOfCreation(LocalDateTime.now());
      invoiceDao.update(invoice);
    } catch (Exception e) {
      logger.error(e.getMessage());
      throw e;
    }
  }

  // CLOSE INVOICE GENERATE NEW AD SET TO CUSTOMER
  // This function close Invoice at current state Set new Invoice to preview
  // customer and establish his Printers
  @Transactional
  public void closeInvoiceAndResetPrinters(Integer invoiceId) {
    try {
      // Find Invoice
      Invoice invoice = invoiceDao.find(invoiceId);
      Objects.requireNonNull(invoice);

      // Close Current Invoice
      invoice.setActual(false);
      invoiceDao.update(invoice);

      // Create new and set Customer
      Customer customer = invoice.getCustomer();
      Invoice newInvoice = new Invoice(customer);

      // Save NEW Invoice
      newInvoice.setQuarterYear(quarterYearService.getNextQuarterYear(invoice.getQuarterYear()));
      invoiceDao.persist(newInvoice);

      // ESTABLISH Customers PRINTERS
      List<Printer> customersPrinters = customer.getPrinters();
      if (customersPrinters != null) {
        customersPrinters.forEach(printerService::EstablishCounts);
      }

    } catch (Exception e) {
      logger.error(e.getMessage());
      throw e;
    }
  }
}
