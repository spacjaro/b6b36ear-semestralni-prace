package cz.cvut.printer_rental.service;

import cz.cvut.printer_rental.dao.BaseDao;
import cz.cvut.printer_rental.dao.RepairmentDao;
import cz.cvut.printer_rental.model.RepairItem;
import cz.cvut.printer_rental.model.Repairment;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class RepairmentService extends AbstractCrudService<Repairment> {
  private static final Logger logger = LoggerFactory.getLogger(RepairmentService.class);
  private final RepairmentDao repairmentDao;
  private final RepairItemService repairItemService;

  // TODO
  @Autowired
  public RepairmentService(RepairmentDao dao, RepairItemService repairItemService) {
    this.repairmentDao = dao;
    this.repairItemService = repairItemService;
  }

  @Override
  protected BaseDao<Repairment> selfDao() {
    return repairmentDao;
  }

  @Override
  public void save(Repairment entity) {
    try {
      super.save(entity);
    } catch (Exception e) {
      logger.error(e.getMessage());
      throw e;
    }
  }

  @Override
  public Repairment findById(Integer id) {
    try {
      return super.findById(id);
    } catch (Exception e) {
      logger.error(e.getMessage());
      return null;
    }
  }

  @Override
  public List<Repairment> findAll() {
    try {
      return super.findAll();
    } catch (Exception e) {
      logger.error(e.getMessage());
      return null;
    }
  }

  @Override
  public void update(Repairment entity) {
    try {
      super.update(entity);
    } catch (Exception e) {
      logger.error(e.getMessage());
      throw e;
    }
  }

  @Override
  public void delete(Repairment entity) {
    try {
      super.delete(entity);
    } catch (Exception e) {
      logger.error(e.getMessage());
      throw e;
    }
  }

  /* NON PERSISTENCE */
  @Transactional
  public Repairment CreateRepairment(List<RepairItem> repairItems, String description, double work_price) {
    Repairment repairment = new Repairment();
    repairment.setDescription(description);
    repairment.setRepairItems(repairItems);
    repairment.setWorkPrice(work_price);
    // super.save(repairment);
    return repairment;

  }

  @Transactional
  public void CalculatePrice(Repairment repairment) {
    double sum = 0.0;

    if (repairment.getRepairItems() != null && !repairment.getRepairItems().isEmpty()) {
      sum = repairment.getRepairItems().stream()
          .mapToDouble(repairItem -> repairItem.getAmount() * repairItem.getPrice_per_unit())
          .sum();
    }
    sum += repairment.getWorkPrice();

    repairment.setTotalPrice(sum);
    repairmentDao.update(repairment);
  }

}
