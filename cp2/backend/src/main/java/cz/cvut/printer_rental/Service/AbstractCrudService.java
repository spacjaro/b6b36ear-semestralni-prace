package cz.cvut.printer_rental.service;

import cz.cvut.printer_rental.dao.BaseDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Objects;

public abstract class AbstractCrudService<T> implements CrudService<T> {

  private static final Logger logger = LoggerFactory.getLogger(AbstractCrudService.class);

  protected abstract BaseDao<T> selfDao();

  @Override
  @Transactional
  public void save(T entity) {
    Objects.requireNonNull(entity, "Entity cannot be null");
    try {
      selfDao().persist(entity);
      logger.info("Saved entity: {}", entity);
    } catch (Exception e) {
      logger.error("Error saving entity: {}", entity, e);
      throw e;
    }
  }

  @Override
  @Transactional(readOnly = true)
  public T findById(Integer id) {
    Objects.requireNonNull(id, "ID cannot be null");
    try {
      T entity = selfDao().find(id);
      return entity;
    } catch (Exception e) {
      logger.error("Error finding entity by ID: {}", id, e);
      throw e;
    }
  }

  @Override
  @Transactional(readOnly = true)
  public List<T> findAll() {
    try {
      List<T> entities = selfDao().findAll();
      return entities;
    } catch (Exception e) {
      logger.error("Error finding all entities", e);
      throw e;
    }
  }

  @Override
  @Transactional
  public void update(T entity) {
    Objects.requireNonNull(entity, "Entity cannot be null");
    try {
      selfDao().update(entity);
      logger.info("Updated entity: {}", entity);
    } catch (Exception e) {
      logger.error("Error updating entity: {}", entity, e);
      throw e;
    }
  }

  @Override
  @Transactional
  public void delete(T entity) {
    Objects.requireNonNull(entity, "Entity cannot be null");
    try {
      selfDao().remove(entity);
      logger.info("Deleted entity: {}", entity);
    } catch (Exception e) {
      logger.error("Error deleting entity: {}", entity, e);
      throw e;
    }
  }
}