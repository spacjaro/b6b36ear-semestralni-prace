package cz.cvut.printer_rental.service;

import cz.cvut.printer_rental.dao.BaseDao;
import cz.cvut.printer_rental.dao.RepairItemDao;
import cz.cvut.printer_rental.model.RepairItem;
import cz.cvut.printer_rental.model.SparePart;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class RepairItemService extends AbstractCrudService<RepairItem> {
  private static final Logger logger = LoggerFactory.getLogger(RepairItemService.class);
  private final RepairItemDao repairItemDao;
  private final SparePartService sparePartService;

  @Autowired
  public RepairItemService(RepairItemDao repairItemDao, SparePartService sparePartService) {
    this.repairItemDao = repairItemDao;
    this.sparePartService = sparePartService;
  }

  @Override
  protected BaseDao<RepairItem> selfDao() {
    return repairItemDao;
  }

  @Override
  @Transactional
  public void save(RepairItem entity) {
    try {
      super.save(entity);
    } catch (Exception e) {
      logger.error(e.getMessage());
      throw e;
    }
  }

  @Override
  @Transactional
  public RepairItem findById(Integer id) {
    try {
      return super.findById(id);
    } catch (Exception e) {
      logger.error(e.getMessage());
      return null;
    }
  }

  @Override
  public List<RepairItem> findAll() {
    try {
      return super.findAll();
    } catch (Exception e) {
      logger.error(e.getMessage());
      return null;
    }
  }

  @Override
  @Transactional
  public void update(RepairItem entity) {
    try {
      super.update(entity);
    } catch (Exception e) {
      logger.error(e.getMessage());
      throw e;
    }
  }

  // TODO test
  @Override
  @Transactional
  public void delete(RepairItem entity) {
    try {
      super.delete(entity);
      sparePartService.increaseAmount(entity.getAmount(), entity.getSparePart().getId());
    } catch (Exception e) {
      logger.error(e.getMessage());
      throw e;
    }
  }

  @Transactional()
  public void setAmount(RepairItem repairItem, int amount) {
    try {
      int newAmount = (repairItem.getAmount() + repairItem.getSparePart().getAmount()) - amount;

      repairItem.getSparePart().setAmount(newAmount);
      repairItem.setAmount(amount);
      repairItemDao.update(repairItem);

    } catch (Exception e) {
      logger.error("Unexpected error in setAmount: {}", e.getMessage());
      throw e;
    }
  }

  // TODO test
  @Transactional
  public RepairItem CreateRepairItem(Integer sparePartId, int amount, double price) {
    try {
      SparePart sparePart = sparePartService.findById(sparePartId);
      sparePartService.decreaseAmount(amount, sparePartId);
      RepairItem repairItem = new RepairItem(sparePart);
      repairItem.setAmount(amount);
      repairItem.setPrice(price);
      super.save(repairItem);
      return repairItem;
    } catch (Exception e) {
      logger.error(e.getMessage());
      return null;
    }
  }
}
