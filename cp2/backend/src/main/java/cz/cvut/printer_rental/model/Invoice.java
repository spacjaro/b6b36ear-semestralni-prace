package cz.cvut.printer_rental.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jakarta.persistence.*;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Entity
@Data
@NoArgsConstructor
@Table(name = "INVOICE")
@NamedQueries({
    // Najde všechny faktury pro konkrétního zákazníka
    @NamedQuery(name = "Invoice.findByCustomer", query = "SELECT i FROM Invoice i WHERE i.customer = :customer"),

    // Najde všechny faktury pro konkrétní čtvrtletí
    @NamedQuery(name = "Invoice.findByQuarterYear", query = "SELECT i FROM Invoice i WHERE i.quarterYear = :quarterYear"),

    // Najde všechny nezaplacené faktury
    @NamedQuery(name = "Invoice.findUnpaid", query = "SELECT i FROM Invoice i WHERE i.isPaid = false"),

    // Najde všechny zaplacené faktury
    @NamedQuery(name = "Invoice.findPaid", query = "SELECT i FROM Invoice i WHERE i.isPaid = true")
})
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class Invoice extends AbstractEntity {

  @Column(name = "is_actual")
  private boolean actual = true;

  @Column(name = "date")
  @Basic(optional = false)
  private LocalDateTime dateOfCreation;

  @ManyToOne
  @JoinColumn(name = "customer_id")
  @JsonBackReference
  private Customer customer;

  @Column
  private Boolean isPaid = false;

  @Column
  private double totalPrice;

  // @OneToMany(mappedBy = "invoice")
  // @JsonManagedReference
  // private List<ServiceRecord> serviceRecords;

  @OneToMany
  private List<PrinterStateCopy> printerStateCopies;

  @ManyToOne
  @JoinColumn(name = "quarter_year_id")
  private QuarterYear quarterYear;

  public Invoice(Customer customer) {
    this.customer = customer;
    this.dateOfCreation = LocalDateTime.now();
    this.isPaid = false;
    this.totalPrice = 0.0;
    // this.serviceRecords = new ArrayList<>();
    this.printerStateCopies = new ArrayList<>();
    this.actual = true;
  }

  @Override
  public String toString() {
    return "Invoice{\n" +
        "   Customer=   "
        + (customer != null ? (customer.getCompanyName() != null ? customer.getCompanyName()
            : customer.getFirstName() + " " + customer.getLastName()) : "No customer")
        + "\n" +
        "   DateOfCreation=" + dateOfCreation + "\n" +
        "   IsPaid=" + (isPaid != null ? isPaid : "No status") + "\n" +
        "   TotalPrice=" + (totalPrice) + "\n" +
        "   Actual=" + actual + "\n" +
        "   QuarterYear=" + (quarterYear != null ? quarterYear.toString() : "No quarter") + "\n" +
        // " ServiceRecords=" + (serviceRecords != null ? serviceRecords.size() : 0) + "
        // service records\n" +
        '}';
  }

  public String toStringSimple() {
    return "Invoice{\n" +
        "   DateOfCreation=" + dateOfCreation + "\n" +
        "   IsPaid=" + (isPaid != null ? isPaid : "No status") + "\n" +
        "   TotalPrice=" + (totalPrice) + "\n" +
        "   Actual=" + actual + "\n" +
        "   QuarterYear=" + (quarterYear != null ? quarterYear.toString() : "No quarter") + "\n" +
        '}';
  }

}
