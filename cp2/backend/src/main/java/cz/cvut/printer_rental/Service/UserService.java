package cz.cvut.printer_rental.service;

import cz.cvut.printer_rental.dao.BaseDao;
import cz.cvut.printer_rental.dao.UserDao;
import cz.cvut.printer_rental.exception.DuplicatePropertyException;
import cz.cvut.printer_rental.model.Admin;
import cz.cvut.printer_rental.model.Mechanic;
import cz.cvut.printer_rental.model.SuperAdmin;
import cz.cvut.printer_rental.model.User;
import cz.cvut.printer_rental.security.SecurityUtils;
import jakarta.persistence.Access;
import jakarta.persistence.EntityNotFoundException;
import jakarta.persistence.NoResultException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
public class UserService extends AbstractCrudService<User> {
  private static final Logger logger = LoggerFactory.getLogger(UserService.class);

  private final UserDao userDao;
  private final PasswordEncoder passwordEncoder;

  @Autowired
  public UserService(UserDao userDao, PasswordEncoder passwordEncoder) {
    this.userDao = userDao;
    this.passwordEncoder = passwordEncoder;
  }

  @Override
  @Transactional
  protected BaseDao<User> selfDao() {
    return userDao;
  }

  @Override
  @Transactional
  public void save(User entity) {
    try {
      checkForDuplicates(entity);
      super.save(entity);
    } catch (DuplicatePropertyException e) {
      logger.warn(e.getMessage());
      throw new DuplicatePropertyException(e.getMessage());
    } catch (Exception e) {
      logger.error(e.getMessage());
      throw e;
    }
  }

  @Override
  @Transactional
  public User findById(Integer id) {
    try {
      return super.findById(id);
    } catch (Exception e) {
      logger.error(e.getMessage());
      return null;
    }
  }

  @Override
  @Transactional
  public List<User> findAll() {
    try {
      return super.findAll();
    } catch (Exception e) {
      logger.error(e.getMessage());
      return null;
    }
  }

  @Override
  @Transactional
  public void update(User entity) {
    try {
      checkForDuplicates(entity);
      super.update(entity);
    } catch (DuplicatePropertyException e) {
      logger.warn(e.getMessage());
      throw new DuplicatePropertyException(e.getMessage());
    } catch (Exception e) {
      logger.error(e.getMessage());
      throw new EntityNotFoundException(e.getMessage());
    }
  }

  @Override
  @Transactional
  public void delete(User entity) {
    try {
      super.delete(entity);
    } catch (Exception e) {
      logger.error(e.getMessage());
      throw new EntityNotFoundException(e.getMessage());
    }
  }

  @Transactional(readOnly = true)
  public User getCurentUser() {
    try {
      return SecurityUtils.getCurrentUser();
    } catch (Exception e) {
      logger.error(e.getMessage());
      return null;
    }
  }

  /**
   * Finds a user by their username.
   * 
   * @param username the username to search for
   * @return an Optional containing the user if found, or an empty Optional
   *         otherwise
   */
  @Transactional
  public User findByUsername(String username) {
    try {
      return userDao.findByUsername(username);
    } catch (EmptyResultDataAccessException e) {
      logger.warn("Empty result");
      return null;
    } catch (NoResultException e) {
      logger.warn("No user found with username: {}", username, e);
      return null;
    } catch (Exception e) {
      logger.error("Error occurred while finding user by username: {}", username, e);
      return null;
    }
  }

  /**
   * Finds a user by their email.
   * 
   * @param email the email to search for
   * @return the user if found, or null otherwise
   */
  @Transactional
  public User findByEmail(String email) {
    Objects.requireNonNull(email);
    try {
      return userDao.findByEmail(email);
    } catch (NoResultException e) {
      logger.warn("No user found with email: {}", email);
    } catch (Exception e) {
      logger.error("Error occurred while finding user by email: {}", email, e);
    }
    return null;
  }

  @Transactional
  public void ChangeRole(Integer userID, String role) {
    Objects.requireNonNull(userID);
    Objects.requireNonNull(role);

    try {
      User current_user = SecurityUtils.getCurrentUser();
      Objects.requireNonNull(current_user);
      User user = userDao.find(userID);

      switch (role) {
        case "ADMIN":
          user = new Admin(user);
          userDao.update(user);
        case "MECHANIC":
          user = new Mechanic(user);
          userDao.update(user);
        case "SUPERADMIN":
          if (current_user instanceof SuperAdmin) {
            user = new SuperAdmin(user);
            userDao.update(user);
          } else {
            logger.warn("SuperAdmin can be instanced only by SuperAdmin");
            throw new SecurityException("SuperAdmin can be instanced only by SuperAdmin");
          }
        default:
          logger.warn("Invalid role: {}", role);
          throw new SecurityException("Invalid role: " + role);
      }
    } catch (EntityNotFoundException e) {
      logger.warn("No user found with ID: {}", userID);
      throw new EntityNotFoundException(e.getMessage());
    } catch (Exception e) {
      logger.error("Error occurred while finding user by ID: {}", userID, e);
      throw new RuntimeException(e);
    }
  }

  @Transactional
  public void changePassword(Integer userId, String password) {
    Objects.requireNonNull(userId);
    try {
      User user = userDao.find(userId);
      if (user instanceof SuperAdmin) {
        logger.warn("Cant change password for SuperAdmin");
        throw new SecurityException("Cant change password for SuperAdmin");
      }
      user.setPassword(password);
      user.encodePassword(passwordEncoder);
      userDao.update(user);
    } catch (EntityNotFoundException e) {
      logger.warn("changePassword:{}", String.valueOf(e));
      throw new EntityNotFoundException(e.getMessage());
    } catch (Exception e) {
      logger.error("Error occurred while changing password for user with ID: {}", userId, e);
      throw new RuntimeException(e);
    }
  }

  private void checkForDuplicates(User entity) {
    Objects.requireNonNull(entity);

    // Check duplicate email
    User existingUserByEmail = null;
    try {
      existingUserByEmail = userDao.findByEmail(entity.getEmail());
    } catch (Exception e) {
      // Log info, že nebyl nalezen uživatel podle emailu
      logger.info("No duplicity found for email: {}", entity.getEmail());
    }

    if (existingUserByEmail != null && !existingUserByEmail.getId().equals(entity.getId())) {
      throw new DuplicatePropertyException("User with email " + entity.getEmail() + " already exists.");
    }

    // Check duplicate username
    User existingUserByUsername = null;
    try {
      existingUserByUsername = userDao.findByUsername(entity.getUsername());
    } catch (Exception e) {
      // Log info, že nebyl nalezen uživatel podle username
      logger.info("No duplicity found for username: {}", entity.getUsername());
    }

    if (existingUserByUsername != null && !existingUserByUsername.getId().equals(entity.getId())) {
      throw new DuplicatePropertyException("User with username " + entity.getUsername() + " already exists.");
    }

    // Check duplicate phone number
    User existingUserByPhone = null;
    try {
      existingUserByPhone = userDao.findByTel(entity.getPhoneNumber());
    } catch (Exception e) {
      // Log info, že nebyl nalezen uživatel podle telefonního čísla
      logger.info("No duplicity found for phone number: {}", entity.getPhoneNumber());
    }

    if (existingUserByPhone != null && !existingUserByPhone.getId().equals(entity.getId())) {
      throw new DuplicatePropertyException("User with phone number " + entity.getPhoneNumber() + " already exists.");
    }
  }

}
