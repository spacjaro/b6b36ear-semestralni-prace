package cz.cvut.printer_rental.dao;

import cz.cvut.printer_rental.model.RepairItem;
import org.springframework.stereotype.Repository;

@Repository
public class RepairItemDao extends BaseDao<RepairItem> {
  public RepairItemDao() {
    super(RepairItem.class);
  }
}
