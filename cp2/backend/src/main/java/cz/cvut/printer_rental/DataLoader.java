package cz.cvut.printer_rental;

import cz.cvut.printer_rental.dao.*;
import cz.cvut.printer_rental.service.*;
import cz.cvut.printer_rental.model.*;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.autoconfigure.info.ProjectInfoProperties;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

@Slf4j
@Component
public class DataLoader implements CommandLineRunner {

  private static final Logger logger = LoggerFactory.getLogger(DataLoader.class);

  private final CustomerService customerService;

  private final PrinterService printerService;

  private final ModelService modelService;

  private final BrandService brandService;

  private final SparePartService sparePartService;

  private final InvoiceService invoiceService;

  private final QuarterYearService quarterYearService;

  private final FacturationService facturationService;

  private final RepairItemService repairItemService;

  private final PrinterServiceFacade printerServiceFacade;

  private final ServiceRecordService serviceRecordService;

  @Autowired
  public DataLoader(CustomerService customerService, PrinterService printerService, ModelService modelService,
      BrandDao brandDao, BrandService brandService, SparePartService sparePartService, InvoiceService invoiceService,
      InvoiceDao invoiceDao, QuarterYearService quarterYearService, FacturationService facturationService,
      ProjectInfoProperties projectInfoProperties, RepairItemService repairItemService,
      PrinterServiceFacade printerServiceFacade, ServiceRecordService serviceRecordService) {
    this.customerService = customerService;
    this.printerService = printerService;
    this.modelService = modelService;
    this.brandService = brandService;
    this.sparePartService = sparePartService;
    this.invoiceService = invoiceService;
    this.quarterYearService = quarterYearService;

    this.facturationService = facturationService;
    this.repairItemService = repairItemService;
    this.printerServiceFacade = printerServiceFacade;
    this.serviceRecordService = serviceRecordService;
  }

  @Override
  @Transactional
  public void run(String... args) throws Exception {
    DataGenerator DG = new DataGenerator();
    Random random = new Random();
    // ******************************* BRAND
    // *************************************************
    List<Brand> brands = DG.generateBrands();
    brands.forEach(brandService::save);

    // ******************************* PRINTER + MODEL
    // *************************************************

    List<PrinterModel> CanonPrinterModels = DG.generatePrinterModels(brands.get(0));
    CanonPrinterModels.forEach(modelService::save);

    List<PrinterModel> HPPrinterModels = DG.generatePrinterModels(brands.get(1));
    HPPrinterModels.forEach(modelService::save);

    List<PrinterModel> MinoltaPrinterModels = DG.generatePrinterModels(brands.get(2));
    MinoltaPrinterModels.forEach(modelService::save);

    List<PrinterModel> printerModels = new ArrayList<>();
    printerModels.addAll(CanonPrinterModels);
    printerModels.addAll(HPPrinterModels);
    printerModels.addAll(MinoltaPrinterModels);

    List<Printer> printers = DG.generatePrinters(printerModels);
    printers.forEach(printerService::save);

    // ******************************* SPARE PART + MODEL
    // *************************************************

    List<SparePartModel> CanonSparePartsModels = DG.generateSparePartModels(brands.get(0));
    CanonSparePartsModels.forEach(modelService::save);

    List<SparePartModel> HPSparePartsModels = DG.generateSparePartModels(brands.get(1));
    HPSparePartsModels.forEach(modelService::save);

    List<SparePartModel> MinoltaSparePartsModels = DG.generateSparePartModels(brands.get(2));
    MinoltaSparePartsModels.forEach(modelService::save);

    List<SparePartModel> sparePartModels = new ArrayList<>();
    sparePartModels.addAll(CanonSparePartsModels);
    sparePartModels.addAll(HPSparePartsModels);
    sparePartModels.addAll(MinoltaSparePartsModels);

    List<SparePart> spareParts = DG.generateSpareParts(sparePartModels);
    spareParts.forEach(sparePartService::save);
    // ******************************* ASSIGN SPARE PARTS TO PRINTER MODELS
    // *************************************************
    Collections.shuffle(sparePartModels);
    for (PrinterModel printerModel : printerModels) {
      for (int i = 0; i < 8; i++) {
        SparePartModel sparePartModel = sparePartModels.get(random.nextInt(0, sparePartModels.size()));
        printerModel.addSparePartModel(sparePartModel);
        sparePartModel.addPrinterModel(printerModel);
        modelService.update(sparePartModel);
      }
      modelService.update(printerModel);
    }
    // ******************************* CUSTOMER
    // *************************************************
    List<Customer> customers = DG.generateCustomers();
    customers.forEach(customerService::save);
    // ******************************* ASSIGN PRINTER TO CUSTOMER
    // *************************************************
    int customerIndex = 0;
    for (Printer printer : printers) {
      Customer actual = customers.get(customerIndex % customers.size());
      customerService.assignPrinterToCustomer(actual.getId(), printer.getId());
      customerIndex++;
    }
    // ******************************* SETTING COUNTS FOR PRINTER
    // *************************************************
    printers.forEach(printer -> {
      try {
        printerService.SetCURRENTNumOfBWCopy(printer.getId(),
            printer.getCURRENTNumOfBWCopy() + random.nextInt(0, 200000));
        printerService.SetCURRENTNumOfColorCopy(printer.getId(),
            printer.getCURRENTNumOfColorCopy() + random.nextInt(0, 200000));
      } catch (Exception e) {
        logger.warn(e.getMessage());
      }
    });
    // ******************************* Create Repairment and add Repairment
    // *************************************************

    Printer myPrinter1 = printers.get(0);
    List<SparePart> spareParts1 = spareParts.subList(0, 5);
    List<SparePart> spareParts2 = spareParts.subList(5, 10);

    try {

      // ******************************* Create two Repairments
      // *************************************************

      List<RepairItem> repairItems = spareParts1.stream()
          .map(sp -> repairItemService.CreateRepairItem(sp.getId(), 1, 100 * random.nextInt(1, 5)))
          .toList();
      printerServiceFacade.createRepairmentForPrinter(myPrinter1.getId(), "Description", 1000, repairItems);

      List<RepairItem> repairItems2 = spareParts2.stream()
          .map(sp -> repairItemService.CreateRepairItem(sp.getId(), 1, 100 * random.nextInt(1, 5)))
          .toList();
      printerServiceFacade.createRepairmentForPrinter(myPrinter1.getId(), "Description", 2000, repairItems2);

      // ******************************* Change Repair Items
      // *************************************************

      for (RepairItem repairItem : repairItems) {
        try {
          repairItemService.setAmount(repairItem, 5);
          serviceRecordService.calculateTotalPrice(serviceRecordService.findActualServiceRecordForPrinter(myPrinter1));
        } catch (IllegalArgumentException e) {
          logger.warn("Validation failed during amount update: {}", e.getMessage());
        }
      }

    } catch (Exception e) {
      logger.warn(e.getMessage());
    }

    // ******************************* CLOSING INVOICES BY QUARTER
    // YEAR*************************************************
    System.out.println("Start closing");
    try {
      QuarterYear quarterYear = quarterYearService.getCurrentQuarterYear();
      System.out.println(quarterYear);
      List<Invoice> invoices = invoiceService.findInvoicesByQuarterYearId(quarterYear.getId());
      System.out.println("Invoice size: " + invoices.size() + "\n");
      invoices.forEach(invoice -> {
        try {
          System.out.println("calculateInvoicePrinters");
          facturationService.calculateInvoicePrinters(invoice.getId());
          System.out.println("closeInvoiceAndResetPrinters");
          facturationService.closeInvoiceAndResetPrinters(invoice.getId());
        } catch (Exception e) {
          logger.error(e.getMessage());
        }
      });
    } catch (Exception e) {
      logger.warn(e.getMessage());
    }

    System.out.println("LOADING HAS FINISHED");

  }
}