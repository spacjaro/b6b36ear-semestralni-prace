# TODOs

V tomto dokumentu budou zaznamenány případné postřehy a požadavky.

## Důležité body k řešení:
1. **Nutnost existence aktuální přiřazené @Invoice v každém momentu existence klienta.**
    - Je potřeba zajistit, aby každý klient měl vždy přiřazenou aktuální fakturu (invoice) v celém jeho životním cyklu v systému.


2. **Automatické generování nového @QuarterYear?**
    - Je třeba doplnit podmínky, za kterých se má v systému automaticky generovat nové období typu QuarterYear.
    - Jaké jsou přesné podmínky pro vytvoření nového čtvrtletního období?


3. **Otázka oboustranného vlastnictví:**
    - Je nutné vyřešit, zda je v systému potřebné oboustranné vlastnictví mezi entitami. Pokud ano, jak to správně implementovat?


4. **Vytvoření výjimek:**
    - Zvážit a implementovat vhodné výjimky pro kritické procesy, kde by standardní tok mohl selhat.
    - Kdy a jak by se měly vyhazovat specifické výjimky?


5. **Přesunutí logiky kalkulace na vyšší vrstvu (@Service):**
    - Je potřeba rozhodnout, zda bude logika kalkulace přesunuta na vyšší vrstvu, konkrétně na úroveň služby (@Service).
    - Současně by se měly mezisoučty uchovávat jako atributy nižších podtříd. Například:
        - `Invoice -> <ServiceRecord> -> <Repairment> -> <RepairItem> = (amount * price)`
    - Jak zajistit správné uchovávání mezisoučtů v těchto entitách?


6. **Absolutní možnost úprav:**
   - Ačkoliv by se něco mohlo zdát kontra produktivní, je potřeba zajistit aby zákazník mohl fakturované údaje a položky libovolně upravovat
   - Klasický CRUD nad všemi položkami aktzální faktury


## NamedQuery

**Customer HOTOVO**
- Najít @Customer dle @companyName
- Najít @Customer dle @firstName
- Najít @Customer dle @lastName
- Najít List<@Customer> dle @QuarterYear


**QuarterYear HOTOVO**
- Najít ACTUAL (ten co se bude fakturovat tento měsíc) @QuarterYear


**Printer HOTOVO**
- Najít List<@Printer> dle @Customer
- Najít List<@Printer> dle @Model
- Najít List<@Printer> dle @Brand


**Invoice HOTOVO**
- Najít List<@Invoice> dle @Customer
- Najít List<@Invoice> dle "ACTUAL" @QuarterYear
- Najít všechny nezaplacené List<@Invoice>
- Najít všechny zaplacené List<@Invoice>

**SparePart HOTOVO**
- Najít všechny @SparePart dle @Printer -> (@Model)